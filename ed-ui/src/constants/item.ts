export const itemNameList: Record<number, string> = {
	// when the corresponding asset exists, the item has been marked with an x in comment
	1: 'potion_irma', // x
	2: 'potion_angel', // x
	3: 'cloud_burger', // x
	4: 'hot_bread', // x
	5: 'meat_pie', // x
	6: 'fight_ration', // x
	7: 'surviving_ration', // x
	8: 'goblin_merguez', // x
	9: 'pampleboum', // x
	10: 'sos_helmet', // x
	11: 'little_pepper', // x
	12: 'zippo', // x
	13: 'sos_flame', // x
	14: 'refrigerated_shield', // x
	15: 'fuca_pill', // x
	16: 'monochromatic', // x
	17: 'poisonite_shot', // x
	18: 'loris_costume', // x
	19: 'vegetox_costume', // x
	20: 'goblin_costume', // x
	21: 'pampleboum_pit', // x
	22: 'portable_love', // x
	23: 'danger_detector', // x
	24: 'pirhanoz_in_bag', // x
	25: 'devil_ointment', // x
	26: 'land_of_ashes', // x
	27: 'abyss', // x
	28: 'amazon', // x
	29: 'st_elmas_fire', // x
	30: 'uvavu', // x
	31: 'strong_tea', // x
	32: 'temporal_stabiliser',
	33: 'elixir', // x
	34: 'elixir_of_life', // x
	35: 'banishement', // x
	36: 'battering_ram', // x
	37: 'ember', // x
	38: 'scale', // x
	39: 'beer', // x
	40: 'encyclopedia', // x
	41: 'antichromatic', // x
	42: 'antidote', // x
	43: 'time_manipulator', // x
	44: 'dimensional_powder', // x
	45: 'sorcerers_stick', // x
	46: 'friendly_whistle', // x
	47: 'dinoz_cube', // x
	48: 'temporal_reduction', // x
	49: 'tear_of_life', // x
	50: 'cuzcussian_mask', // x
	51: 'anti_grave_suit', // x
	52: 'enchanted_steroid', // x
	53: 'curse_locker', // x
	54: 'fear_factor', // x
	55: 'life_stealer', // x
	56: 'fire_sphere', // x
	57: 'wood_sphere', // x
	58: 'water_sphere', // x
	59: 'lightning_sphere', // x
	60: 'air_sphere', // x
	61: 'demon_ticket', // x
	62: 'treasure_coupon', // x
	63: 'moueffe_egg',
	64: 'moueffe_egg_rare', // x
	65: 'pigmou_egg',
	66: 'pigmou_egg_rare', // x
	67: 'winks_egg',
	68: 'winks_egg_rare', // x
	69: 'planaille_egg',
	70: 'planaille_egg_rare',
	71: 'castivore_egg',
	72: 'castivore_egg_rare', // x
	73: 'rocky_egg',
	74: 'rocky_egg_rare', // x
	75: 'pteroz_egg',
	76: 'pteroz_egg_rare',
	77: 'nuagoz_egg',
	78: 'nuagoz_egg_rare', // x
	79: 'sirain_egg',
	80: 'sirain_egg_rare', // x
	81: 'hippoclamp_egg',
	82: 'hippoclamp_egg_rare',
	83: 'gorilloz_egg',
	84: 'gorilloz_egg_rare', // x
	85: 'wanwan_egg',
	86: 'wanwan_egg_rare', // x
	87: 'wanwan_baby_rare', // x
	88: 'santaz_egg', // x
	89: 'santaz_egg_rare', // x
	90: 'feross_egg', // x
	91: 'feross_egg_rare', // x
	92: 'feross_egg_christmas', // x
	93: 'kabuki_egg', // x
	94: 'kabuki_egg_rare', // x
	95: 'mahamuti_egg', // x
	96: 'mahamuti_egg_rare', // x
	97: 'souffet_egg', // x
	98: 'soufflet_egg_rare',
	99: 'toufufu_baby', // x
	100: 'toufufu_baby_rare', // x
	101: 'quetzu_egg',
	102: 'quetzu_egg_rare', // x
	103: 'smog_egg', // x
	104: 'smog_egg_anniversary', // x
	105: 'smog_egg_christmas_blue', // x
	106: 'smog_egg_christmas_green', // x
	107: 'triceragnon_baby', // x
	108: 'triceragnon_baby_rare',
	109: 'amnesic_rice', // x
	110: 'tik_bracelet', // x
	111: 'magic_star', // x
	112: 'golden_napodino', // x
	113: 'bamboo_friend', // x
	114: 'candle_card', // x
	115: 'christmas_ticket', // x
	116: 'tictac_ticket', // x
	117: 'anniversary_ticket', // x
	118: 'easter_egg', // x
	119: 'fire_cracker', // x
	120: 'special_potion_irma', // x
	998: 'empty', // x
	999: 'undefined' // x
};
