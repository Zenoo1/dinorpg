export const moueffe: Moueffe = {
	head: {
		0: {
			imgNumber: 0,
			default: {
				top: 0,
				left: -3
			},
			dependentImage: {
				imgNumber: 0,
				top: 20,
				left: 17,
				zIndex: 2
			},
			minorInjury: {
				imgNumber: 0,
				top: 11,
				left: 35
			},
			heavyInjury: {
				imgNumber: 0,
				top: 2,
				left: 28
			},
			eye: {
				baby: {
					imgNumber: 3,
					top: 7,
					left: 40
				},
				adult: {
					imgNumber: 4,
					top: 8,
					left: 40
				}
			},
			hair: {
				1: {
					top: -3,
					left: 36,
					zIndex: -1,
					hasTailHair: true
				},
				2: {
					top: -8,
					left: 36,
					zIndex: -1,
					hasTailHair: true
				},
				3: {
					top: -9,
					left: 27,
					zIndex: -1,
					hasTailHair: true
				},
				4: {
					top: -3,
					left: 42,
					zIndex: -1
				},
				5: {
					top: -16,
					left: 32,
					zIndex: -1,
					dependentImage: {
						imgNumber: 11,
						top: -3,
						left: 52.5
					},
					dependentImage2: {
						imgNumber: 10,
						top: -4.5,
						left: 48,
						zIndex: 1
					}
				},
				6: {
					top: -14,
					left: 33,
					zIndex: -1,
					dependentImage: {
						imgNumber: 12,
						top: 3,
						left: 48
					}
				},
				7: {
					top: 3.5,
					left: 55,
					zIndex: 2,
					dependentImage2: {
						imgNumber: 8,
						top: -5,
						left: 34,
						zIndex: 0
					}
				}
			},
			tattoo: {
				1: {
					imgNumber: 0,
					top: 10.5,
					left: 44
				},
				2: {
					imgNumber: 0,
					top: 10.5,
					left: 44
				},
				3: {
					imgNumber: 1,
					top: 12,
					left: 46
				},
				4: {
					imgNumber: 1,
					top: 12,
					left: 46
				},
				5: {
					imgNumber: 2,
					top: 12,
					left: 44
				},
				6: {
					imgNumber: 2,
					top: 12,
					left: 44
				}
			}
		},
		1: {
			imgNumber: 1,
			default: {
				top: 0,
				left: 0
			},
			minorInjury: {
				imgNumber: 0,
				top: 11,
				left: 33
			},
			heavyInjury: {
				imgNumber: 1,
				top: 2,
				left: 17.5
			},
			eye: {
				baby: {
					imgNumber: 7,
					top: 6,
					left: 29
				},
				adult: {
					imgNumber: 8,
					top: 6,
					left: 29
				}
			},
			hair: {
				1: {
					top: -4,
					left: 31,
					zIndex: -1,
					hasTailHair: true
				},
				2: {
					top: -8,
					left: 32,
					zIndex: -1,
					hasTailHair: true
				},
				3: {
					top: -9,
					left: 23,
					zIndex: -1,
					hasTailHair: true
				},
				4: {
					top: -3,
					left: 37,
					zIndex: -1
				},
				5: {
					top: -16,
					left: 28,
					zIndex: -1,
					dependentImage: {
						imgNumber: 11,
						top: -3,
						left: 48.5
					},
					dependentImage2: {
						imgNumber: 10,
						top: -5,
						left: 43,
						zIndex: 1
					}
				},
				6: {
					top: -14,
					left: 29,
					zIndex: -1,
					dependentImage: {
						imgNumber: 12,
						top: 2,
						left: 44
					}
				},
				7: {
					top: 3.5,
					left: 51,
					zIndex: 2,
					dependentImage2: {
						imgNumber: 8,
						top: -5,
						left: 29.5,
						zIndex: 0
					}
				}
			},
			tattoo: {
				1: {
					imgNumber: 0,
					top: 11.5,
					left: 40
				},
				2: {
					imgNumber: 0,
					top: 11.5,
					left: 40
				},
				3: {
					imgNumber: 1,
					top: 12,
					left: 43
				},
				4: {
					imgNumber: 1,
					top: 12,
					left: 43
				},
				5: {
					imgNumber: 2,
					top: 11.5,
					left: 41
				},
				6: {
					imgNumber: 2,
					top: 11.5,
					left: 41
				}
			}
		},
		2: {
			imgNumber: 1,
			default: {
				top: 0,
				left: 0
			},
			dependentImage: {
				imgNumber: 1,
				top: 32,
				left: 6,
				zIndex: 0
			},
			minorInjury: {
				imgNumber: 0,
				top: 11,
				left: 33
			},
			heavyInjury: {
				imgNumber: 1,
				top: 2,
				left: 17.5
			},
			eye: {
				baby: {
					imgNumber: 7,
					top: 6,
					left: 29
				},
				adult: {
					imgNumber: 8,
					top: 6,
					left: 29
				}
			},
			hair: {
				1: {
					top: -4,
					left: 31,
					zIndex: -1,
					hasTailHair: true
				},
				2: {
					top: -8,
					left: 32,
					zIndex: -1,
					hasTailHair: true
				},
				3: {
					top: -9,
					left: 23,
					zIndex: -1,
					hasTailHair: true
				},
				4: {
					top: -3,
					left: 37,
					zIndex: -1
				},
				5: {
					top: -16,
					left: 28,
					zIndex: -1,
					dependentImage: {
						imgNumber: 11,
						top: -3,
						left: 48.5
					},
					dependentImage2: {
						imgNumber: 10,
						top: -5,
						left: 43,
						zIndex: 1
					}
				},
				6: {
					top: -14,
					left: 29,
					zIndex: -1,
					dependentImage: {
						imgNumber: 12,
						top: 2,
						left: 44
					}
				},
				7: {
					top: 3.5,
					left: 51,
					zIndex: 2,
					dependentImage2: {
						imgNumber: 8,
						top: -5,
						left: 29.5,
						zIndex: 0
					}
				}
			},
			tattoo: {
				1: {
					imgNumber: 0,
					top: 11.5,
					left: 40
				},
				2: {
					imgNumber: 0,
					top: 11.5,
					left: 40
				},
				3: {
					imgNumber: 1,
					top: 12,
					left: 43
				},
				4: {
					imgNumber: 1,
					top: 12,
					left: 43
				},
				5: {
					imgNumber: 2,
					top: 11.5,
					left: 41
				},
				6: {
					imgNumber: 2,
					top: 11.5,
					left: 41
				}
			}
		},
		3: {
			imgNumber: 2,
			default: {
				top: -1,
				left: 3
			},
			dependentImage2: {
				imgNumber: 4,
				top: 13,
				left: 32
			},
			minorInjury: {
				imgNumber: 2,
				top: 13,
				left: 17
			},
			heavyInjury: {
				imgNumber: 2,
				top: 2,
				left: 15
			},
			eye: {
				baby: {
					imgNumber: 0,
					top: 5.3,
					left: 28.2
				},
				adult: {
					imgNumber: 1,
					top: 5.5,
					left: 28.5
				}
			},
			hair: {
				1: {
					top: -1,
					left: 26,
					zIndex: -1,
					hasTailHair: true
				},
				2: {
					top: -6,
					left: 28,
					zIndex: -1,
					hasTailHair: true
				},
				3: {
					top: -7,
					left: 20,
					zIndex: -1,
					hasTailHair: true
				},
				4: {
					top: -1,
					left: 33,
					zIndex: -1
				},
				5: {
					top: -14.5,
					left: 23,
					zIndex: -1,
					dependentImage: {
						imgNumber: 11,
						top: -1,
						left: 44
					},
					dependentImage2: {
						imgNumber: 10,
						top: -3,
						left: 38,
						zIndex: 1
					}
				},
				6: {
					top: -11,
					left: 24.5,
					zIndex: -1,
					dependentImage: {
						imgNumber: 12,
						top: 3,
						left: 40
					}
				},
				7: {
					top: 5,
					left: 46,
					zIndex: 2,
					dependentImage2: {
						imgNumber: 8,
						top: -4,
						left: 24,
						zIndex: 0
					}
				}
			},
			tattoo: {
				1: {
					imgNumber: 0,
					top: 13,
					left: 36
				},
				2: {
					imgNumber: 0,
					top: 13,
					left: 36
				},
				3: {
					imgNumber: 1,
					top: 13.5,
					left: 37
				},
				4: {
					imgNumber: 1,
					top: 13.5,
					left: 37
				},
				5: {
					imgNumber: 2,
					top: 13,
					left: 35.5
				},
				6: {
					imgNumber: 2,
					top: 13,
					left: 35.5
				}
			}
		},
		4: {
			imgNumber: 2,
			default: {
				top: -1,
				left: 3
			},
			dependentImage: {
				imgNumber: 2,
				top: 36,
				left: -2,
				zIndex: 0
			},
			dependentImage2: {
				imgNumber: 4,
				top: 13,
				left: 32.5
			},
			minorInjury: {
				imgNumber: 2,
				top: 13,
				left: 17
			},
			heavyInjury: {
				imgNumber: 2,
				top: 2,
				left: 15
			},
			eye: {
				baby: {
					imgNumber: 0,
					top: 5.3,
					left: 28.2
				},
				adult: {
					imgNumber: 1,
					top: 5.5,
					left: 28.5
				}
			},
			hair: {
				1: {
					top: -1,
					left: 26,
					zIndex: -1,
					hasTailHair: true
				},
				2: {
					top: -6,
					left: 28,
					zIndex: -1,
					hasTailHair: true
				},
				3: {
					top: -7,
					left: 20,
					zIndex: -1,
					hasTailHair: true
				},
				4: {
					top: -1,
					left: 33,
					zIndex: -1
				},
				5: {
					top: -14.5,
					left: 23,
					zIndex: -1,
					dependentImage: {
						imgNumber: 11,
						top: -1,
						left: 44
					},
					dependentImage2: {
						imgNumber: 10,
						top: -3,
						left: 38,
						zIndex: 1
					}
				},
				6: {
					top: -11,
					left: 24.5,
					zIndex: -1,
					dependentImage: {
						imgNumber: 12,
						top: 3,
						left: 40
					}
				},
				7: {
					top: 5,
					left: 46,
					zIndex: 2,
					dependentImage2: {
						imgNumber: 8,
						top: -4,
						left: 24,
						zIndex: 0
					}
				}
			},
			tattoo: {
				1: {
					imgNumber: 0,
					top: 13,
					left: 36
				},
				2: {
					imgNumber: 0,
					top: 13,
					left: 36
				},
				3: {
					imgNumber: 1,
					top: 13.5,
					left: 37
				},
				4: {
					imgNumber: 1,
					top: 13.5,
					left: 37
				},
				5: {
					imgNumber: 2,
					top: 13,
					left: 35.5
				},
				6: {
					imgNumber: 2,
					top: 13,
					left: 35.5
				}
			}
		},
		5: {
			imgNumber: 3,
			default: {
				top: -1,
				left: -3
			},
			minorInjury: {
				imgNumber: 1,
				top: 14.5,
				left: 5
			},
			heavyInjury: {
				imgNumber: 3,
				top: 2,
				left: 18
			},
			eye: {
				baby: {
					imgNumber: 11,
					top: 4.5,
					left: 33
				},
				adult: {
					imgNumber: 12,
					top: 5.5,
					left: 35
				}
			},
			eyeRetina: {
				imgNumber: 10,
				top: 5.5,
				left: 32
			},
			hair: {
				1: {
					top: -1,
					left: 35,
					zIndex: -1,
					hasTailHair: true
				},
				2: {
					top: -5,
					left: 37,
					zIndex: -1,
					hasTailHair: true
				},
				3: {
					top: -7,
					left: 28,
					zIndex: -1,
					hasTailHair: true
				},
				4: {
					top: -2,
					left: 41,
					zIndex: -1
				},
				5: {
					top: -15,
					left: 31,
					zIndex: -1,
					dependentImage: {
						imgNumber: 11,
						top: -1,
						left: 53
					},
					dependentImage2: {
						imgNumber: 10,
						top: -3.7,
						left: 46,
						zIndex: 0
					}
				},
				6: {
					top: -12,
					left: 33,
					zIndex: -1,
					dependentImage: {
						imgNumber: 12,
						top: 2.5,
						left: 48
					}
				},
				7: {
					top: 5,
					left: 54.5,
					zIndex: 2,
					dependentImage2: {
						imgNumber: 8,
						top: -4,
						left: 31.5,
						zIndex: 0
					}
				}
			},
			tattoo: {
				1: {
					imgNumber: 0,
					top: 13,
					left: 44
				},
				2: {
					imgNumber: 0,
					top: 13,
					left: 44
				},
				3: {
					imgNumber: 1,
					top: 13.5,
					left: 46.5
				},
				4: {
					imgNumber: 1,
					top: 13.5,
					left: 46.5
				},
				5: {
					imgNumber: 2,
					top: 13.5,
					left: 45
				},
				6: {
					imgNumber: 2,
					top: 13.5,
					left: 45
				}
			}
		},
		6: {
			imgNumber: 3,
			default: {
				top: -1,
				left: -3
			},
			dependentImage: {
				imgNumber: 3,
				top: 13,
				left: 1,
				zIndex: 2
			},
			minorInjury: {
				imgNumber: 0,
				top: 11,
				left: 35
			},
			heavyInjury: {
				imgNumber: 3,
				top: 2,
				left: 18
			},
			eye: {
				baby: {
					imgNumber: 11,
					top: 4.5,
					left: 33
				},
				adult: {
					imgNumber: 12,
					top: 5.5,
					left: 35
				}
			},
			eyeRetina: {
				imgNumber: 10,
				top: 5.5,
				left: 32
			},
			hair: {
				1: {
					top: -1,
					left: 35,
					zIndex: -1,
					hasTailHair: true
				},
				2: {
					top: -5,
					left: 37,
					zIndex: -1,
					hasTailHair: true
				},
				3: {
					top: -7,
					left: 28,
					zIndex: -1,
					hasTailHair: true
				},
				4: {
					top: -2,
					left: 41,
					zIndex: -1
				},
				5: {
					top: -15,
					left: 31,
					zIndex: -1,
					dependentImage: {
						imgNumber: 11,
						top: -1,
						left: 53
					},
					dependentImage2: {
						imgNumber: 10,
						top: -3.7,
						left: 46,
						zIndex: 0
					}
				},
				6: {
					top: -12,
					left: 33,
					zIndex: -1,
					dependentImage: {
						imgNumber: 12,
						top: 2.5,
						left: 48
					}
				},
				7: {
					top: 5,
					left: 54.5,
					zIndex: 2,
					dependentImage2: {
						imgNumber: 8,
						top: -4,
						left: 31.5,
						zIndex: 0
					}
				}
			},
			tattoo: {
				1: {
					imgNumber: 0,
					top: 13,
					left: 44
				},
				2: {
					imgNumber: 0,
					top: 13,
					left: 44
				},
				3: {
					imgNumber: 1,
					top: 13.5,
					left: 46.5
				},
				4: {
					imgNumber: 1,
					top: 13.5,
					left: 46.5
				},
				5: {
					imgNumber: 2,
					top: 13.5,
					left: 45
				},
				6: {
					imgNumber: 2,
					top: 13.5,
					left: 45
				}
			}
		},
		7: {
			imgNumber: 4,
			default: {
				top: -3,
				left: 1
			},
			minorInjury: {
				imgNumber: 0,
				top: 13,
				left: 34
			},
			heavyInjury: {
				imgNumber: 4,
				top: 3,
				left: 5
			},
			eye: {
				baby: {
					imgNumber: 15,
					top: 8,
					left: 28.5
				},
				adult: {
					imgNumber: 16,
					top: 8.5,
					left: 29
				}
			},
			eyeRetina: {
				imgNumber: 14,
				top: 7.5,
				left: 28
			},
			hair: {
				1: {
					top: -1,
					left: 32,
					zIndex: -1,
					hasTailHair: true
				},
				2: {
					top: -5,
					left: 33,
					zIndex: -1,
					hasTailHair: true
				},
				3: {
					top: -7,
					left: 25,
					zIndex: -1,
					hasTailHair: true
				},
				4: {
					top: -1,
					left: 40,
					zIndex: -1
				},
				5: {
					top: -13,
					left: 29,
					zIndex: -1,
					dependentImage: {
						imgNumber: 11,
						top: 0,
						left: 50
					},
					dependentImage2: {
						imgNumber: 10,
						top: -2,
						left: 44,
						zIndex: 0
					}
				},
				6: {
					top: -10,
					left: 30,
					zIndex: -1,
					dependentImage: {
						imgNumber: 12,
						top: 4,
						left: 46
					}
				},
				7: {
					top: 6,
					left: 53,
					zIndex: 2,
					dependentImage2: {
						imgNumber: 8,
						top: -3,
						left: 32,
						zIndex: 0
					}
				}
			},
			tattoo: {
				1: {
					imgNumber: 0,
					top: 13,
					left: 40.5
				},
				2: {
					imgNumber: 0,
					top: 13,
					left: 40.5
				},
				3: {
					imgNumber: 1,
					top: 13.5,
					left: 42
				},
				4: {
					imgNumber: 1,
					top: 13.5,
					left: 42
				},
				5: {
					imgNumber: 2,
					top: 13.5,
					left: 41.5
				},
				6: {
					imgNumber: 2,
					top: 13.5,
					left: 41.5
				}
			}
		}
	},
	chest: {
		default: {
			top: 17,
			left: 24
		},
		0: {
			element1: {
				imgNumber: 0,
				top: 0,
				left: 0
			},
			element2: {
				imgNumber: 1,
				top: 2.5,
				left: 1
			},
			minorInjury: {
				1: {
					imgNumber: 0,
					top: 35,
					left: 24
				},
				2: {
					imgNumber: 1,
					top: 30.5,
					left: 5.5
				}
			},
			heavyInjury: {
				1: {
					imgNumber: 0,
					top: 24,
					left: 3
				},
				2: {
					imgNumber: 1,
					top: 23,
					left: 15
				}
			},
			tail: {
				size: 0.8,
				top: 40,
				left: 27
			},
			tailSpecial: {
				top: 40,
				left: 19
			},
			tailHair: {
				1: {
					size: 0.7,
					top: 42.5,
					left: 44,
					rotation: -14
				},
				2: {
					size: 0.7,
					top: 40.5,
					left: 43,
					rotation: -12
				},
				3: {
					size: 0.7,
					top: 37.7,
					left: 42.4,
					rotation: -14
				}
			},
			tattoo: {
				top: 23,
				left: 1
			}
		},
		1: {
			element1: {
				imgNumber: 0,
				top: 0,
				left: 0
			},
			element2: {
				imgNumber: 1,
				top: 2.5,
				left: 1
			},
			minorInjury: {
				1: {
					imgNumber: 0,
					top: 35,
					left: 24
				},
				2: {
					imgNumber: 1,
					top: 30.5,
					left: 5.5
				}
			},
			heavyInjury: {
				1: {
					imgNumber: 0,
					top: 24,
					left: 3
				},
				2: {
					imgNumber: 1,
					top: 23,
					left: 15
				}
			},
			tail: {
				size: 1.2,
				top: 31,
				left: 16
			},
			tailSpecial: {
				top: 40,
				left: 19
			},
			tailHair: {
				1: {
					size: 1.03,
					top: 35.2,
					left: 42,
					rotation: -14
				},
				2: {
					size: 1.03,
					top: 32.2,
					left: 40,
					rotation: -10
				},
				3: {
					size: 0.95,
					top: 30,
					left: 42,
					rotation: -17
				}
			},
			tattoo: {
				top: 23,
				left: 1
			}
		},
		2: {
			element1: {
				imgNumber: 2,
				top: 0,
				left: 0
			},
			element2: {
				imgNumber: 3,
				top: 3.5,
				left: 3
			},
			element3: {
				imgNumber: 4,
				top: 4.5,
				left: 0
			},
			minorInjury: {
				1: {
					imgNumber: 0,
					top: 34,
					left: 24
				},
				2: {
					imgNumber: 1,
					top: 22,
					left: 4
				}
			},
			heavyInjury: {
				1: {
					imgNumber: 0,
					top: 18,
					left: 4
				},
				2: {
					imgNumber: 2,
					top: 32,
					left: 24
				},
				3: {
					imgNumber: 3,
					top: 31,
					left: 21
				}
			},
			tail: {
				size: 0.8,
				top: 40,
				left: 27
			},
			tailSpecial: {
				top: 38,
				left: 19
			},
			tailHair: {
				1: {
					size: 0.7,
					top: 42.5,
					left: 44,
					rotation: -14
				},
				2: {
					size: 0.7,
					top: 40.5,
					left: 43,
					rotation: -12
				},
				3: {
					size: 0.7,
					top: 37.7,
					left: 42.4,
					rotation: -14
				}
			}
		},
		3: {
			element1: {
				imgNumber: 2,
				top: 0,
				left: 0
			},
			element2: {
				imgNumber: 3,
				top: 3.5,
				left: 3
			},
			element3: {
				imgNumber: 4,
				top: 4.5,
				left: 0
			},
			minorInjury: {
				1: {
					imgNumber: 0,
					top: 34,
					left: 24
				},
				2: {
					imgNumber: 1,
					top: 22,
					left: 4
				}
			},
			heavyInjury: {
				1: {
					imgNumber: 0,
					top: 18,
					left: 4
				},
				2: {
					imgNumber: 2,
					top: 32,
					left: 24
				},
				3: {
					imgNumber: 3,
					top: 31,
					left: 21
				}
			},
			tail: {
				size: 1.2,
				top: 31,
				left: 20
			},
			tailSpecial: {
				top: 38,
				left: 19
			},
			tailHair: {
				1: {
					size: 1.1,
					top: 34.5,
					left: 42,
					rotation: -8
				},
				2: {
					size: 1.1,
					top: 31.6,
					left: 42,
					rotation: -8
				},
				3: {
					size: 1,
					top: 29,
					left: 42,
					rotation: -12
				}
			}
		}
	},
	arm: {
		0: {
			imgNumber: 0,
			left: {
				default: {
					top: 2,
					left: -6
				},
				minorInjury: {
					imgNumber: 0,
					top: 23.5,
					left: 1
				},
				heavyInjury: {
					imgNumber: 0,
					top: 5.5,
					left: 0
				},
				dependentImage0: {
					imgNumber: 5,
					top: 12,
					left: 6,
					zIndex: 0
				}
			},
			right: {
				default: {
					top: -4,
					left: 64
				},
				minorInjury: {
					imgNumber: 0,
					top: 10,
					left: 38
				},
				heavyInjury: {
					imgNumber: 0,
					top: 6,
					left: 38
				},
				dependentImage0: {
					imgNumber: 6,
					top: 15,
					left: 40
				},
				tattoo: {
					1: {
						imgNumber: 0,
						top: 15,
						left: 39.4
					},
					2: {
						imgNumber: 0,
						top: 15,
						left: 39.4
					},
					3: {
						imgNumber: 1,
						top: 15,
						left: 43.2
					},
					4: {
						imgNumber: 1,
						top: 15,
						left: 43.2
					},
					5: {
						imgNumber: 2,
						top: 8,
						left: 41
					},
					6: {
						imgNumber: 2,
						top: 8,
						left: 41
					},
					7: {
						imgNumber: 3,
						top: 13,
						left: 42
					},
					8: {
						imgNumber: 3,
						top: 13,
						left: 42
					},
					9: {
						imgNumber: 4,
						top: 20,
						left: 50
					},
					A: {
						imgNumber: 4,
						top: 20,
						left: 50
					}
				}
			}
		},
		1: {
			imgNumber: 0,
			left: {
				default: {
					top: 2,
					left: -8
				},
				minorInjury: {
					imgNumber: 0,
					top: 23.5,
					left: 1
				},
				heavyInjury: {
					imgNumber: 0,
					top: 5.5,
					left: 0
				},
				dependentImage0: {
					imgNumber: 5,
					top: 12,
					left: 6,
					zIndex: 0
				},
				dependentImage1: {
					imgNumber: 4,
					top: 14,
					left: 40
				}
			},
			right: {
				default: {
					top: -4,
					left: 64
				},
				minorInjury: {
					imgNumber: 0,
					top: 10,
					left: 38
				},
				heavyInjury: {
					imgNumber: 0,
					top: 6,
					left: 38
				},
				dependentImage0: {
					imgNumber: 6,
					top: 15,
					left: 40
				},
				dependentImage1: {
					imgNumber: 5,
					top: 13,
					left: 3,
					zIndex: 0
				},
				tattoo: {
					1: {
						imgNumber: 0,
						top: 15,
						left: 39.4
					},
					2: {
						imgNumber: 0,
						top: 15,
						left: 39.4
					},
					3: {
						imgNumber: 1,
						top: 15,
						left: 43.2
					},
					4: {
						imgNumber: 1,
						top: 15,
						left: 43.2
					},
					5: {
						imgNumber: 2,
						top: 8,
						left: 41
					},
					6: {
						imgNumber: 2,
						top: 8,
						left: 41
					},
					7: {
						imgNumber: 3,
						top: 13,
						left: 42
					},
					8: {
						imgNumber: 3,
						top: 13,
						left: 42
					},
					9: {
						imgNumber: 4,
						top: 20,
						left: 50
					},
					A: {
						imgNumber: 4,
						top: 20,
						left: 50
					}
				}
			}
		},
		2: {
			imgNumber: 0,
			left: {
				default: {
					top: 2,
					left: -8
				},
				minorInjury: {
					imgNumber: 0,
					top: 23.5,
					left: 1
				},
				heavyInjury: {
					imgNumber: 0,
					top: 5.5,
					left: 0
				},
				dependentImage0: {
					imgNumber: 5,
					top: 12,
					left: 6,
					zIndex: 0
				},
				dependentImage1: {
					imgNumber: 1,
					top: 15,
					left: -2
				},
				dependentImage2: {
					baby: {
						imgNumber: 2,
						top: 16,
						left: -1.5
					},
					adult: {
						imgNumber: 3,
						top: 26.5,
						left: -2
					}
				}
			},
			right: {
				default: {
					top: -4,
					left: 64
				},
				minorInjury: {
					imgNumber: 0,
					top: 10,
					left: 38
				},
				heavyInjury: {
					imgNumber: 0,
					top: 6,
					left: 38
				},
				dependentImage0: {
					imgNumber: 6,
					top: 15,
					left: 40
				},
				dependentImage1: {
					imgNumber: 1,
					top: 10.5,
					left: 49.5,
					zIndex: 3
				},
				dependentImage2: {
					baby: {
						imgNumber: 3,
						top: 16,
						left: 53.5
					},
					adult: {
						imgNumber: 4,
						top: 33,
						left: 55.5
					}
				},
				dependentImage3: {
					imgNumber: 2,
					top: 32.7,
					left: 55.5
				}
			}
		},
		3: {
			imgNumber: 0,
			left: {
				default: {
					top: 2,
					left: -8
				},
				minorInjury: {
					imgNumber: 0,
					top: 23.5,
					left: 1
				},
				heavyInjury: {
					imgNumber: 0,
					top: 5.5,
					left: 0
				},
				dependentImage0: {
					imgNumber: 5,
					top: 12,
					left: 6,
					zIndex: 0
				},
				dependentImage1: {
					imgNumber: 1,
					top: 15,
					left: -2
				},
				dependentImage2: {
					baby: {
						imgNumber: 2,
						top: 16,
						left: -1.5
					},
					adult: {
						imgNumber: 3,
						top: 26.5,
						left: -2
					}
				},
				dependentImage4: {
					imgNumber: 4,
					top: 14,
					left: 40
				}
			},
			right: {
				default: {
					top: -4,
					left: 64
				},
				minorInjury: {
					imgNumber: 0,
					top: 10,
					left: 38
				},
				heavyInjury: {
					imgNumber: 0,
					top: 6,
					left: 38
				},
				dependentImage0: {
					imgNumber: 6,
					top: 15,
					left: 40
				},
				dependentImage1: {
					imgNumber: 1,
					top: 10.5,
					left: 49.5,
					zIndex: 3
				},
				dependentImage2: {
					baby: {
						imgNumber: 3,
						top: 16,
						left: 53.5
					},
					adult: {
						imgNumber: 4,
						top: 33,
						left: 55.5
					}
				},
				dependentImage3: {
					imgNumber: 2,
					top: 32.7,
					left: 55.5
				},
				dependentImage4: {
					imgNumber: 5,
					top: 13,
					left: 3
				}
			}
		},
		4: {
			imgNumber: 1,
			left: {
				default: {
					top: 20,
					left: -8
				}
			},
			right: {
				default: {
					top: 18,
					left: 61
				}
			}
		},
		5: {
			imgNumber: 1,
			left: {
				default: {
					top: 20,
					left: -8
				},
				dependentImage0: {
					imgNumber: 0,
					top: 13,
					left: 0,
					zIndex: -2
				}
			},
			right: {
				default: {
					top: 18,
					left: 61
				},
				dependentImage0: {
					imgNumber: 0,
					top: 13,
					left: 20
				}
			}
		}
	},
	leg: {
		0: {
			left: {
				default: {
					top: 60,
					left: -8
				},
				nail: {
					top: 5,
					left: -3
				},
				minorInjury: {
					1: {
						imgNumber: 0,
						top: 11,
						left: 20
					}
				},
				heavyInjury: {
					1: {
						imgNumber: 0,
						top: 6,
						left: 31
					},
					2: {
						imgNumber: 1,
						top: 13,
						left: 18
					}
				},
				knee: {
					horn: {
						imgNumber: 1,
						top: -2,
						left: 18
					},
					skin: {
						imgNumber: 0,
						top: 1,
						left: 18
					}
				}
			},
			right: {
				default: {
					top: 58,
					left: 56.5
				},
				nail: {
					top: 10,
					left: 18
				},
				minorInjury: {
					1: {
						imgNumber: 0,
						top: 24,
						left: 26
					}
				},
				heavyInjury: {
					1: {
						imgNumber: 0,
						top: 22,
						left: 11
					},
					2: {
						imgNumber: 1,
						top: 19,
						left: 31
					},
					3: {
						imgNumber: 2,
						top: 21,
						left: 22
					}
				},
				knee: {
					horn: {
						imgNumber: 1,
						top: 5.5,
						left: 16
					},
					skin: {
						imgNumber: 0,
						top: 7.5,
						left: 14
					}
				}
			}
		}
	},
	tattoo: {
		1: {
			hasChestTattoo: false
		},
		2: {
			hasChestTattoo: true
		},
		3: {
			hasChestTattoo: false
		},
		4: {
			hasChestTattoo: true
		},
		5: {
			hasChestTattoo: false
		},
		6: {
			hasChestTattoo: true
		},
		7: {
			hasChestTattoo: false
		},
		8: {
			hasChestTattoo: true
		},
		9: {
			hasChestTattoo: false
		},
		A: {
			hasChestTattoo: true
		}
	},
	color: {
		body: {
			0: {
				baby: {
					mainFirstColor: '#fff0a7',
					mainSecondColor: '#e5b86f',
					mainThirdColor: '#d0944b',
					chestFirstColor: '#ffffd7',
					chestSecondColor: '#ffeca3',
					chestThirdColor: '#ffbe75',
					chestFourthColor: '#fffdb4',
					chestFifthColor: '#f9db92',
					chestSixthColor: '#ffd188',
					chestSeventhColor: '#eaa960',
					borderColor: '#af694e'
				},
				adult: {
					mainFirstColor: '#f0dc97',
					mainSecondColor: '#d2a863',
					mainThirdColor: '#be8540',
					chestFirstColor: '#ffffd4',
					chestSecondColor: '#ffd893',
					chestThirdColor: '#eead68',
					chestFourthColor: '#ffe8a3',
					chestFifthColor: '#e3c782',
					chestSixthColor: '#ecc07b',
					chestSeventhColor: '#d69a54',
					borderColor: '#984828'
				}
			},
			1: {
				baby: {
					mainFirstColor: '#ffdf84',
					mainSecondColor: '#f6a74c',
					mainThirdColor: '#e18328',
					chestFirstColor: '#ffffbb',
					chestSecondColor: '#ffdb80',
					chestThirdColor: '#ffad53',
					chestFourthColor: '#ffec91',
					chestFifthColor: '#ffc96f',
					chestSixthColor: '#ffbe64',
					chestSeventhColor: '#fa983d',
					borderColor: '#ac5435'
				},
				adult: {
					mainFirstColor: '#f3b761',
					mainSecondColor: '#e29742',
					mainThirdColor: '#ce7520',
					chestFirstColor: '#ffffb3',
					chestSecondColor: '#ffc772',
					chestThirdColor: '#fe9d48',
					chestFourthColor: '#ffd882',
					chestFifthColor: '#f3b761',
					chestSixthColor: '#fcaf5a',
					chestSeventhColor: '#e68934',
					borderColor: '#ab4813'
				}
			},
			2: {
				baby: {
					mainFirstColor: '#ffb81f',
					mainSecondColor: '#f68302',
					mainThirdColor: '#e25f03',
					chestFirstColor: '#fff159',
					chestSecondColor: '#ffb636',
					chestThirdColor: '#ff892d',
					chestFourthColor: '#ffc72e',
					chestFifthColor: '#ffa50c',
					chestSixthColor: '#ff9c1b',
					chestSeventhColor: '#fb7418',
					borderColor: '#b0503e'
				},
				adult: {
					mainFirstColor: '#ffa919',
					mainSecondColor: '#e27500',
					mainThirdColor: '#ce5300',
					chestFirstColor: '#ffe656',
					chestSecondColor: '#ffa52c',
					chestThirdColor: '#ff7b24',
					chestFourthColor: '#ffb626',
					chestFifthColor: '#f49404',
					chestSixthColor: '#fc8d13',
					chestSeventhColor: '#e66710',
					borderColor: '#830800'
				}
			},
			3: {
				baby: {
					mainFirstColor: '#e77528',
					mainSecondColor: '#d45407',
					mainThirdColor: '#c02f04',
					chestFirstColor: '#ffc779',
					chestSecondColor: '#ff873a',
					chestThirdColor: '#f35b2e',
					chestFourthColor: '#ff984b',
					chestFifthColor: '#e57225',
					chestSixthColor: '#f06e1f',
					chestSeventhColor: '#da4519',
					borderColor: '#983e35'
				},
				adult: {
					mainFirstColor: '#e17d34',
					mainSecondColor: '#c24900',
					mainThirdColor: '#af2700',
					chestFirstColor: '#ffb970',
					chestSecondColor: '#f87930',
					chestThirdColor: '#df4e25',
					chestFourthColor: '#f48a41',
					chestFifthColor: '#d4681f',
					chestSixthColor: '#dc6117',
					chestSeventhColor: '#c73a11',
					borderColor: '#740000'
				}
			},
			4: {
				baby: {
					mainFirstColor: '#d6f393',
					mainSecondColor: '#c2d271',
					mainThirdColor: '#aeae4d',
					chestFirstColor: '#ffffe5',
					chestSecondColor: '#fcffa4',
					chestThirdColor: '#e1d977',
					chestFourthColor: '#f7ffb6',
					chestFifthColor: '#d7f493',
					chestSixthColor: '#dfec8a',
					chestSeventhColor: '#c8c362',
					borderColor: '#955f32'
				},
				adult: {
					mainFirstColor: '#d0f398',
					mainSecondColor: '#b2c065',
					mainThirdColor: '#9e9e42',
					chestFirstColor: '#ffffd6',
					chestSecondColor: '#e7f094',
					chestThirdColor: '#cec66a',
					chestFourthColor: '#e0fda2',
					chestFifthColor: '#c3df83',
					chestSixthColor: '#ccd87c',
					chestSeventhColor: '#b6b256',
					borderColor: '#8a5e24'
				}
			},
			5: {
				baby: {
					mainFirstColor: '#bed965',
					mainSecondColor: '#acb945',
					mainThirdColor: '#979420',
					chestFirstColor: '#ffffb8',
					chestSecondColor: '#e4eb77',
					chestThirdColor: '#cabe4b',
					chestFourthColor: '#dffc88',
					chestFifthColor: '#bdd662',
					chestSixthColor: '#c8d15e',
					chestSeventhColor: '#b0a936',
					borderColor: '#914c35'
				},
				adult: {
					mainFirstColor: '#badb6e',
					mainSecondColor: '#9ca73a',
					mainThirdColor: '#888518',
					chestFirstColor: '#f7ffaa',
					chestSecondColor: '#d1d76a',
					chestThirdColor: '#b8ad40',
					chestFourthColor: '#cde77a',
					chestFifthColor: '#aec659',
					chestSixthColor: '#b6bf52',
					chestSeventhColor: '#a0992c',
					borderColor: '#844828'
				}
			},
			6: {
				baby: {
					mainFirstColor: '#98c8ff',
					mainSecondColor: '#85a8e1',
					mainThirdColor: '#7083bb',
					chestFirstColor: '#dfffff',
					chestSecondColor: '#bddaff',
					chestThirdColor: '#a3ade5',
					chestFourthColor: '#b9ebff',
					chestFifthColor: '#96c5fc',
					chestSixthColor: '#a1c0f9',
					chestSeventhColor: '#8998d0',
					borderColor: '#58395f'
				},
				adult: {
					mainFirstColor: '#96cbff',
					mainSecondColor: '#7797cd',
					mainThirdColor: '#6375aa',
					chestFirstColor: '#d2ffff',
					chestSecondColor: '#acc7fc',
					chestThirdColor: '#939dd2',
					chestFourthColor: '#aad9ff',
					chestFifthColor: '#89b6eb',
					chestSixthColor: '#91afe4',
					chestSeventhColor: '#7c89be',
					borderColor: '#69475f'
				}
			},
			7: {
				baby: {
					mainFirstColor: '#99b3ec',
					mainSecondColor: '#797cb5',
					mainThirdColor: '#645890',
					chestFirstColor: '#d6efff',
					chestSecondColor: '#b1afe7',
					chestThirdColor: '#9783bb',
					chestFourthColor: '#acc0f8',
					chestFifthColor: '#8c9ed6',
					chestSixthColor: '#9596ce',
					chestSeventhColor: '#7d6ea6',
					borderColor: '#462862'
				},
				adult: {
					mainFirstColor: '#8aa3d8',
					mainSecondColor: '#6b6fa4',
					mainThirdColor: '#584c82',
					chestFirstColor: '#baccff',
					chestSecondColor: '#a19fd4',
					chestThirdColor: '#8874aa',
					chestFourthColor: '#9db0e5',
					chestFifthColor: '#7d8ec3',
					chestSixthColor: '#8687bc',
					chestSeventhColor: '#706096',
					borderColor: '#2f0d43'
				}
			},
			8: {
				baby: {
					mainFirstColor: '#e77528',
					mainSecondColor: '#d45407',
					mainThirdColor: '#c02f04',
					chestFirstColor: '#ffc779',
					chestSecondColor: '#ff873a',
					chestThirdColor: '#f35b2e',
					chestFourthColor: '#ff984b',
					chestFifthColor: '#e57225',
					chestSixthColor: '#f06e1f',
					chestSeventhColor: '#da4519',
					borderColor: '#a02e23'
				},
				adult: {
					mainFirstColor: '#d4681f',
					mainSecondColor: '#c24900',
					mainThirdColor: '#af2700',
					chestFirstColor: '#ffb970',
					chestSecondColor: '#f87930',
					chestThirdColor: '#df4e25',
					chestFourthColor: '#f48940',
					chestFifthColor: '#d4681f',
					chestSixthColor: '#dc6117',
					chestSeventhColor: '#c73a11',
					borderColor: '#932922'
				}
			},
			9: {
				baby: {
					mainFirstColor: '#be540c',
					mainSecondColor: '#ac3404',
					mainThirdColor: '#960e05',
					chestFirstColor: '#ffa65e',
					chestSecondColor: '#e36637',
					chestThirdColor: '#ca392f',
					chestFourthColor: '#db722a',
					chestFifthColor: '#bc510b',
					chestSixthColor: '#c74d1d',
					chestSeventhColor: '#b0231a',
					borderColor: '#710d08'
				},
				adult: {
					mainFirstColor: '#ba5e1a',
					mainSecondColor: '#9b2a00',
					mainThirdColor: '#870800',
					chestFirstColor: '#f69a56',
					chestSecondColor: '#d05a2e',
					chestThirdColor: '#b73026',
					chestFourthColor: '#cd6a26',
					chestFifthColor: '#ad4905',
					chestSixthColor: '#b54216',
					chestSeventhColor: '#9f1c12',
					borderColor: '#2f0d43'
				}
			},
			A: {
				baby: {
					mainFirstColor: '#d44d4d',
					mainSecondColor: '#c12c2c',
					mainThirdColor: '#ac0707',
					chestFirstColor: '#ff9a9a',
					chestSecondColor: '#f95e5e',
					chestThirdColor: '#df3131',
					chestFourthColor: '#f46f6f',
					chestFifthColor: '#d24a4a',
					chestSixthColor: '#dd4444',
					chestSeventhColor: '#c51c1c',
					borderColor: '#891916'
				},
				adult: {
					mainFirstColor: '#ce5757',
					mainSecondColor: '#b02222',
					mainThirdColor: '#9c0000',
					chestFirstColor: '#ff9393',
					chestSecondColor: '#e55252',
					chestThirdColor: '#cc2828',
					chestFourthColor: '#e16363',
					chestFifthColor: '#c14242',
					chestSixthColor: '#ca3a3a',
					chestSeventhColor: '#b41414',
					borderColor: '#892b24'
				}
			},
			B: {
				baby: {
					mainFirstColor: '#fff8a7',
					mainSecondColor: '#f5d986',
					mainThirdColor: '#e0b461',
					chestFirstColor: '#fffff6',
					chestSecondColor: '#ffffb9',
					chestThirdColor: '#ffde8b',
					chestFourthColor: '#ffffc3',
					chestFifthColor: '#fff9a7',
					chestSixthColor: '#fff19f',
					chestSeventhColor: '#f9c876',
					borderColor: '#a76042'
				},
				adult: {
					mainFirstColor: '#fff9ac',
					mainSecondColor: '#e0c577',
					mainThirdColor: '#cda355',
					chestFirstColor: '#ffffe9',
					chestSecondColor: '#fdca7d',
					chestThirdColor: '#fdca7d',
					chestFourthColor: '#ffffb8',
					chestFifthColor: '#f2e497',
					chestSixthColor: '#fbdd8f',
					chestSeventhColor: '#e5b669',
					borderColor: '#9d542e'
				}
			}
		},
		hair: {
			0: {
				hairFirstColor: '#f2ddc9',
				hairSecondColor: '#e1beab',
				hairThirdColor: '#cd9b88',
				hairBorderColor: '#a25948'
			},
			1: {
				hairFirstColor: '#f2e497',
				hairSecondColor: '#e1c678',
				hairThirdColor: '#cda355',
				hairBorderColor: '#9c532f'
			},
			2: {
				hairFirstColor: '#f49404',
				hairSecondColor: '#e37600',
				hairThirdColor: '#ce5300',
				hairBorderColor: '#a43524'
			},
			3: {
				hairFirstColor: '#8a1900',
				hairSecondColor: '#790100',
				hairThirdColor: '#640000',
				hairBorderColor: '#4c0000'
			},
			4: {
				hairFirstColor: '#efb800',
				hairSecondColor: '#de9900',
				hairThirdColor: '#ca7600',
				hairBorderColor: '#9e482e'
			},
			5: {
				hairFirstColor: '#415269',
				hairSecondColor: '#31344a',
				hairThirdColor: '#1c1027',
				hairBorderColor: '#281a1a'
			},
			6: {
				hairFirstColor: '#d4681f',
				hairSecondColor: '#c24900',
				hairThirdColor: '#af2700',
				hairBorderColor: '#981906'
			},
			7: {
				hairFirstColor: '#ad4905',
				hairSecondColor: '#9b2a00',
				hairThirdColor: '#870800',
				hairBorderColor: '#711100'
			}
		},
		tattoo: {
			0: '#feedd4',
			1: '#ffb94a',
			2: '#ff8e0f',
			3: '#f7fecb',
			4: '#eafe72',
			5: '#ede3fe',
			6: '#d5b9ff',
			7: '#d185cb',
			8: '#f55522',
			9: '#e53f11',
			A: '#f10e0e',
			B: '#fef791',
			C: '#f9d076'
		}
	}
};

interface Moueffe {
	head: {
		[headNumber: string]: {
			imgNumber: number;
			default: {
				top: number;
				left: number;
			};
			dependentImage?: {
				imgNumber: number;
				top: number;
				left: number;
				zIndex: number;
			};
			dependentImage2?: {
				imgNumber: number;
				top: number;
				left: number;
			};
			minorInjury?: {
				imgNumber: number;
				top: number;
				left: number;
			};
			heavyInjury?: {
				imgNumber: number;
				top: number;
				left: number;
			};
			eye: {
				baby: {
					imgNumber: number;
					top: number;
					left: number;
				};
				adult: {
					imgNumber: number;
					top: number;
					left: number;
				};
			};
			eyeRetina?: {
				imgNumber: number;
				top: number;
				left: number;
			};
			hair: {
				[hairNumber: string]: {
					top: number;
					left: number;
					zIndex: number;
					hasTailHair?: boolean;
					dependentImage?: {
						imgNumber: number;
						top: number;
						left: number;
					};
					dependentImage2?: {
						imgNumber: number;
						top: number;
						left: number;
						zIndex: number;
					};
				};
			};
			tattoo?: {
				[tattooNumber: string]: {
					imgNumber: number;
					top: number;
					left: number;
				};
			};
		};
	};
	chest: {
		default: {
			top: number;
			left: number;
		};
		[chestNumber: number]: {
			element1: {
				imgNumber: number;
				top: number;
				left: number;
			};
			element2: {
				imgNumber: number;
				top: number;
				left: number;
			};
			element3?: {
				imgNumber: number;
				top: number;
				left: number;
			};
			minorInjury: {
				[minorInjuryNumber: string]: {
					imgNumber: number;
					top: number;
					left: number;
				};
			};
			heavyInjury: {
				[heavyInjuryNumber: string]: {
					imgNumber: number;
					top: number;
					left: number;
				};
			};
			tail: {
				size: number;
				top: number;
				left: number;
			};
			tailSpecial: {
				top: number;
				left: number;
			};
			tailHair: {
				[tailHairNumber: string]: {
					size: number;
					top: number;
					left: number;
					rotation: number;
				};
			};
			tattoo?: {
				top: number;
				left: number;
			};
		};
	};
	arm: {
		[armNumber: string]: {
			imgNumber: number;
			left: {
				default: {
					top: number;
					left: number;
				};
				minorInjury?: {
					imgNumber: number;
					top: number;
					left: number;
				};
				heavyInjury?: {
					imgNumber: number;
					top: number;
					left: number;
				};
				dependentImage0?: {
					imgNumber: number;
					top: number;
					left: number;
					zIndex: number;
				};
				dependentImage1?: {
					imgNumber: number;
					top: number;
					left: number;
				};
				dependentImage2?: {
					baby: {
						imgNumber: number;
						top: number;
						left: number;
					};
					adult: {
						imgNumber: number;
						top: number;
						left: number;
					};
				};
				dependentImage4?: {
					imgNumber: number;
					top: number;
					left: number;
				};
			};
			right: {
				default: {
					top: number;
					left: number;
				};
				minorInjury?: {
					imgNumber: number;
					top: number;
					left: number;
				};
				heavyInjury?: {
					imgNumber: number;
					top: number;
					left: number;
				};
				dependentImage0?: {
					imgNumber: number;
					top: number;
					left: number;
				};
				dependentImage1?: {
					imgNumber: number;
					top: number;
					left: number;
					zIndex: number;
				};
				dependentImage2?: {
					baby: {
						imgNumber: number;
						top: number;
						left: number;
					};
					adult: {
						imgNumber: number;
						top: number;
						left: number;
					};
				};
				dependentImage3?: {
					imgNumber: number;
					top: number;
					left: number;
				};
				dependentImage4?: {
					imgNumber: number;
					top: number;
					left: number;
				};
				tattoo?: {
					[tattooNumber: string]: {
						imgNumber: number;
						top: number;
						left: number;
					};
				};
			};
		};
	};
	leg: {
		[legNumber: string]: {
			left: {
				default: {
					top: number;
					left: number;
				};
				nail: {
					top: number;
					left: number;
				};
				minorInjury: {
					[minorInjuryNumber: string]: {
						imgNumber: number;
						top: number;
						left: number;
					};
				};
				heavyInjury: {
					[heavyInjuryNumber: string]: {
						imgNumber: number;
						top: number;
						left: number;
					};
				};
				knee: {
					horn: {
						imgNumber: number;
						top: number;
						left: number;
					};
					skin: {
						imgNumber: number;
						top: number;
						left: number;
					};
				};
			};
			right: {
				default: {
					top: number;
					left: number;
				};
				nail: {
					top: number;
					left: number;
				};
				minorInjury: {
					[minorInjuryNumber: string]: {
						imgNumber: number;
						top: number;
						left: number;
					};
				};
				heavyInjury: {
					[heavyInjuryNumber: string]: {
						imgNumber: number;
						top: number;
						left: number;
					};
				};
				knee: {
					horn: {
						imgNumber: number;
						top: number;
						left: number;
					};
					skin: {
						imgNumber: number;
						top: number;
						left: number;
					};
				};
			};
		};
	};
	tattoo: {
		[tattooNumber: string]: {
			hasChestTattoo: boolean;
		};
	};
	color: {
		body: {
			[colorNumber: string]: {
				baby: {
					mainFirstColor: string;
					mainSecondColor: string;
					mainThirdColor: string;
					chestFirstColor: string;
					chestSecondColor: string;
					chestThirdColor: string;
					chestFourthColor: string;
					chestFifthColor: string;
					chestSixthColor: string;
					chestSeventhColor: string;
					borderColor: string;
				};
				adult: {
					mainFirstColor: string;
					mainSecondColor: string;
					mainThirdColor: string;
					chestFirstColor: string;
					chestSecondColor: string;
					chestThirdColor: string;
					chestFourthColor: string;
					chestFifthColor: string;
					chestSixthColor: string;
					chestSeventhColor: string;
					borderColor: string;
				};
			};
		};
		hair: {
			[colorNumber: string]: {
				hairFirstColor: string;
				hairSecondColor: string;
				hairThirdColor: string;
				hairBorderColor: string;
			};
		};
		tattoo: {
			[colorNumber: string]: string;
		};
	};
}
