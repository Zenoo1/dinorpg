import { FightResult } from '@drpg/core/models/fight/FightResult';
import { AxiosError } from 'axios';
import mitt from 'mitt';
import { DinozItems } from '@drpg/core/models/item/DinozItems';

type Events = {
	responseError: AxiosError;
	isLoading: boolean;
	fightResult: FightResult;
	resurrect: boolean;
	toast: toast;
	refreshDinoz: boolean;
	refreshMoney: boolean;
	refreshInventory: boolean;
	equipItem: Array<DinozItems>;
};

type toast = {
	message: string;
	type: 'error' | 'reward';
};

const EventBus = mitt<Events>();

export default EventBus;
