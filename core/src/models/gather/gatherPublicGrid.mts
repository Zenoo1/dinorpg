export type GatherPublicGrid = {
	grid: Array<Array<-1 | 0>>;
	gatherTurn: number;
	gatherType: string;
};
