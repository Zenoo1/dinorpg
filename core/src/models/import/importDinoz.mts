export interface ImportDinoz {
	skills: Array<number>;
	status: Array<number>;
	name: string;
	isSacrificed: boolean;
	isFrozen: boolean;
	level: number;
	life: number;
	maxLife: number;
	experience: number;
	nbrUpFire: number;
	nbrUpWood: number;
	nbrUpWater: number;
	nbrUpLightning: number;
	nbrUpAir: number;
	raceId: number;
}
