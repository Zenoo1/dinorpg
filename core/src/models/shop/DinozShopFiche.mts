import { DinozRace } from '../dinoz/DinozRace.mjs';

export interface DinozShopFiche {
	id: string;
	display: string;
	race: DinozRace;
}
