export interface PlayerEdit {
	customText?: string;
	hasImported?: boolean;
	rewards?: Array<string>;
	epicOperation?: string;
	money?: number;
	operation?: string;
	quetzuBought?: number;
	leader?: boolean;
	engineer?: boolean;
	cooker?: boolean;
	shopKeeper?: boolean;
	merchant?: boolean;
	priest?: boolean;
	teacher?: boolean;
}
