export interface NewPositions {
	id: number;
	sumPosition: number;
	averagePosition: number;
	sumPointsDisplayed: number;
	averagePointsDisplayed: number;
	dinozCountDisplayed: number;
}
