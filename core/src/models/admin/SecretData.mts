export interface SecretData {
	key: string;
	value: string;
}
