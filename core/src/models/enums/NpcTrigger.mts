export enum NpcTrigger {
	QUEST = 'quest',
	ALWAYS = 'always',
	MISSION = 'mission'
}
