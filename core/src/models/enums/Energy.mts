export enum Energy {
	VHIGH = '75',
	HIGH = '50',
	NORMAL = '25',
	WEAK = '10',
	NONE = '0'
}
