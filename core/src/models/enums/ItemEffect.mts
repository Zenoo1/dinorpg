export enum ItemEffect {
	HEAL = 'heal',
	RESURRECT = 'resurrect',
	EGG = 'egg',
	ACTION = 'action',
	SPHERE = 'sphere',
	GOLD = 'gold',
	SPECIAL = 'special'
}
