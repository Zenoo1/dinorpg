export enum SkillEffect {
	CHANGE_MAX_LIFE = 'changeMaxLife',
	CHANGE_ELEMENT = 'changeElement'
}
