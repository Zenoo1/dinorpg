export enum ShopType {
	CLASSIC = 'classic',
	MAGICAL = 'magical',
	CURSED = 'cursed'
}
