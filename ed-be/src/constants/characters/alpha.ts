import { ConditionEnum, RewardEnum } from '@drpg/core/models/enums/Parser';
import { NpcData } from '@drpg/core/models/npc/NpcData';
import { ElementType } from '@drpg/core/models/enums/ElementType';
import { statusList } from '../status.js';

export const ALPHA: Readonly<Record<string, NpcData>> = {
	begin: {
		stepName: 'begin',
		nextStep: ['talk'],
		initialStep: true
	},
	talk: {
		stepName: 'talk',
		alias: 'back',
		nextStep: ['element', 'world', 'experience']
	},
	element: {
		stepName: 'element',
		condition: {
			conditionType: ConditionEnum.MAXLEVEL,
			value: 80
		},
		nextStep: ['fire', 'water', 'lightning', 'wood', 'air']
	},
	fire: {
		stepName: 'fire',
		nextStep: ['back'],
		reward: [
			{
				rewardType: RewardEnum.CHANGE_ELEMENT,
				value: ElementType.FIRE
			}
		]
	},
	water: {
		stepName: 'water',
		nextStep: ['back'],
		reward: [
			{
				rewardType: RewardEnum.CHANGE_ELEMENT,
				value: ElementType.WATER
			}
		]
	},
	lightning: {
		stepName: 'lightning',
		nextStep: ['back'],
		reward: [
			{
				rewardType: RewardEnum.CHANGE_ELEMENT,
				value: ElementType.LIGHTNING
			}
		]
	},
	wood: {
		stepName: 'wood',
		nextStep: ['back'],
		reward: [
			{
				rewardType: RewardEnum.CHANGE_ELEMENT,
				value: ElementType.WOOD
			}
		]
	},
	air: {
		stepName: 'air',
		nextStep: ['back'],
		reward: [
			{
				rewardType: RewardEnum.CHANGE_ELEMENT,
				value: ElementType.AIR
			}
		]
	},
	world: {
		stepName: 'world',
		nextStep: [
			'nothing',
			'GO_TO_GRAND_TOUT_CHAUD',
			'GO_TO_ATLANTEINES_ISLAND',
			'CIMETIERE',
			'GO_TO_DINOPLAZA',
			'GO_TO_MONSTER_ISLAND',
			'GO_TO_FOREST',
			'GO_TO_DOME_SOULAFLOTTE',
			'GO_TO_TUNNEL',
			'JUNGLE_SAUVAGE',
			'GO_TO_STEPPES'
		]
	},
	nothing: {
		stepName: 'nothing',
		nextStep: ['back']
	},
	GO_TO_GRAND_TOUT_CHAUD: {
		stepName: 'GO_TO_GRAND_TOUT_CHAUD',
		nextStep: ['back'],
		condition: {
			conditionType: ConditionEnum.STATUS,
			value: statusList.CLIMBING_GEAR,
			reverse: true
		},
		reward: [
			{
				rewardType: RewardEnum.STATUS,
				value: statusList.CLIMBING_GEAR
			}
		]
	},
	GO_TO_ATLANTEINES_ISLAND: {
		stepName: 'GO_TO_ATLANTEINES_ISLAND',
		nextStep: ['back'],
		condition: {
			conditionType: ConditionEnum.STATUS,
			value: statusList.BUOY,
			reverse: true
		},
		reward: [
			{
				rewardType: RewardEnum.STATUS,
				value: statusList.BUOY
			}
		]
	},
	CIMETIERE: {
		stepName: 'CIMETIERE',
		nextStep: ['back'],
		condition: {
			conditionType: ConditionEnum.STATUS,
			value: statusList.SKULLY_MEMORY,
			reverse: true
		},
		reward: [
			{
				rewardType: RewardEnum.STATUS,
				value: statusList.SKULLY_MEMORY
			}
		]
	},
	GO_TO_DINOPLAZA: {
		stepName: 'GO_TO_DINOPLAZA',
		nextStep: ['back'],
		condition: {
			conditionType: ConditionEnum.STATUS,
			value: statusList.DINOPLAZA,
			reverse: true
		},
		reward: [
			{
				rewardType: RewardEnum.STATUS,
				value: statusList.DINOPLAZA
			}
		]
	},
	GO_TO_MONSTER_ISLAND: {
		stepName: 'GO_TO_MONSTER_ISLAND',
		nextStep: ['back'],
		condition: {
			conditionType: ConditionEnum.STATUS,
			value: statusList.JOVEBOZE,
			reverse: true
		},
		reward: [
			{
				rewardType: RewardEnum.STATUS,
				value: statusList.JOVEBOZE
			}
		]
	},
	GO_TO_FOREST: {
		stepName: 'GO_TO_FOREST',
		nextStep: ['back'],
		condition: {
			conditionType: ConditionEnum.STATUS,
			value: statusList.NENUPHAR_LEAF,
			reverse: true
		},
		reward: [
			{
				rewardType: RewardEnum.STATUS,
				value: statusList.NENUPHAR_LEAF
			}
		]
	},
	GO_TO_DOME_SOULAFLOTTE: {
		stepName: 'GO_TO_DOME_SOULAFLOTTE',
		nextStep: ['back'],
		condition: {
			conditionType: ConditionEnum.STATUS,
			value: statusList.RASCAPHANDRE_DECOY,
			reverse: true
		},
		reward: [
			{
				rewardType: RewardEnum.STATUS,
				value: statusList.RASCAPHANDRE_DECOY
			}
		]
	},
	GO_TO_TUNNEL: {
		stepName: 'GO_TO_TUNNEL',
		nextStep: ['back'],
		condition: {
			conditionType: ConditionEnum.STATUS,
			value: statusList.LANTERN,
			reverse: true
		},
		reward: [
			{
				rewardType: RewardEnum.STATUS,
				value: statusList.LANTERN
			}
		]
	},
	JUNGLE_SAUVAGE: {
		stepName: 'JUNGLE_SAUVAGE',
		nextStep: ['back'],
		condition: {
			conditionType: ConditionEnum.STATUS,
			value: statusList.FLIPPERS,
			reverse: true
		},
		reward: [
			{
				rewardType: RewardEnum.STATUS,
				value: statusList.FLIPPERS
			}
		]
	},
	GO_TO_STEPPES: {
		stepName: 'GO_TO_STEPPES',
		nextStep: ['back'],
		condition: {
			conditionType: ConditionEnum.STATUS,
			value: statusList.SYLVENOIRE_KEY,
			reverse: true
		},
		reward: [
			{
				rewardType: RewardEnum.STATUS,
				value: statusList.SYLVENOIRE_KEY
			}
		]
	},
	experience: {
		stepName: 'experience',
		condition: {
			conditionType: ConditionEnum.MAXLEVEL,
			value: 80
		},
		nextStep: ['maxExperience']
	},
	maxExperience: {
		stepName: 'maxExperience',
		nextStep: ['back'],
		reward: [
			{
				rewardType: RewardEnum.MAXEXPERIENCE,
				value: 1
			}
		]
	},
	stop: {
		stepName: 'stop',
		nextStep: []
	}
};
