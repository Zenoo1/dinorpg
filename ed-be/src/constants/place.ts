import { statusList } from './status.js';
import { Place } from '@drpg/core/models/place/Place';
import { MapZone } from '@drpg/core/models/enums/MapZone';
import { GatherType } from '@drpg/core/models/enums/GatherType';
import { ConditionEnum, ConditionOperatorEnum } from '@drpg/core/models/enums/Parser';

export const placeList: Record<string, Place> = {
	// Useful for the few things accessible from any where like the flying shop
	ANYWHERE: {
		placeId: 0,
		name: 'anywhere',
		borderPlace: [],
		map: MapZone.ALL
	},
	PORT_DE_PRECHE: {
		placeId: 1,
		name: 'port',
		borderPlace: [7, 10, 11, 13],
		map: MapZone.DINOLAND,
		gather: GatherType.FISH,
		specialGather: GatherType.ANNIV
	},
	PLACE_DU_MARCHE: {
		placeId: 2,
		name: 'market',
		borderPlace: [4],
		map: MapZone.DINOLAND
	},
	PAPY_JOE: {
		placeId: 3,
		name: 'papy',
		borderPlace: [4, 6, 7],
		map: MapZone.DINOLAND,
		gather: GatherType.HUNT
	},
	FORCEBRUT: {
		placeId: 4,
		name: 'forcebrut',
		borderPlace: [2, 3, 7],
		map: MapZone.DINOLAND,
		gather: GatherType.ENERGY1
	},
	DINOVILLE: {
		placeId: 5,
		name: 'dnv',
		borderPlace: [6, 7, 12],
		map: MapZone.DINOLAND,
		gather: GatherType.SEEK
	},
	UNIVERSITE: {
		placeId: 6,
		name: 'universite',
		borderPlace: [3, 5, 8],
		map: MapZone.DINOLAND,
		gather: GatherType.CUEILLE1
	},
	FOUTAINE_DE_JOUVENCE: {
		placeId: 7,
		name: 'fountj',
		borderPlace: [1, 3, 4, 5],
		map: MapZone.DINOLAND,
		gather: GatherType.CUEILLE1
	},
	COLLINES_ESCARPEES: {
		placeId: 8,
		name: 'colesc',
		borderPlace: [6, 9],
		map: MapZone.DINOLAND,
		gather: GatherType.HUNT
	},
	GO_TO_GRAND_TOUT_CHAUD: {
		placeId: 9,
		name: 'gogtc',
		borderPlace: [8],
		alias: 43,
		conditions: {
			conditionType: ConditionEnum.STATUS,
			value: statusList.CLIMBING_GEAR
		},
		map: MapZone.GTOUTCHAUD
	},
	GO_TO_ATLANTEINES_ISLAND: {
		placeId: 10,
		name: 'goiles',
		borderPlace: [1],
		alias: 31,
		conditions: {
			conditionType: ConditionEnum.STATUS,
			value: statusList.BUOY
		},
		map: MapZone.ILES
	},
	CIMETIERE: {
		placeId: 11,
		name: 'skull',
		borderPlace: [1],
		conditions: {
			conditionType: ConditionEnum.STATUS,
			value: statusList.SKULLY_MEMORY
		},
		map: MapZone.DINOLAND
	},
	GO_TO_DINOPLAZA: {
		placeId: 12,
		name: 'goplaz',
		borderPlace: [5],
		alias: 35,
		conditions: {
			conditionType: ConditionEnum.STATUS,
			value: statusList.DINOPLAZA
		},
		map: MapZone.DINOWEST
	},
	GO_TO_MONSTER_ISLAND: {
		placeId: 13,
		name: 'gomisl',
		conditions: {
			conditionType: ConditionEnum.STATUS,
			value: statusList.JOVEBOZE
		},
		borderPlace: [1],
		alias: 92,
		map: MapZone.ILEMONSTRE
	},
	AUREE_DE_LA_FORET: {
		placeId: 14,
		name: 'auree',
		borderPlace: [15, 23],
		map: MapZone.JUNGLE,
		gather: GatherType.CUEILLE1
	},
	CHEMIN_GLAUQUE: {
		placeId: 15,
		name: 'chemin',
		borderPlace: [14, 16, 17],
		map: MapZone.JUNGLE,
		gather: GatherType.CUEILLE1
	},
	COLLINES_HANTEES: {
		placeId: 16,
		name: 'collin',
		borderPlace: [15, 17],
		map: MapZone.JUNGLE,
		gather: GatherType.SEEK
	},
	FLEUVE_JUMIN: {
		placeId: 17,
		name: 'fleuve',
		borderPlace: [15, 16, 19, 18],
		map: MapZone.JUNGLE,
		gather: GatherType.FISH
	},
	FLEUVE_JUMIN_BIS: {
		placeId: 102,
		name: 'fleuve',
		borderPlace: [18],
		conditions: {
			conditionType: ConditionEnum.STATUS,
			value: statusList.FLIPPERS
		},
		alias: 17,
		map: MapZone.JUNGLE
	},
	CAMP_KORGON: {
		placeId: 18,
		name: 'camp',
		borderPlace: [22, 102],
		conditions: {
			conditionType: ConditionEnum.STATUS,
			value: statusList.FLIPPERS
		},
		map: MapZone.JUNGLE,
		gather: GatherType.HUNT
	},
	JUNGLE_SAUVAGE: {
		placeId: 19,
		name: 'jungle',
		borderPlace: [17, 20],
		conditions: {
			conditionType: ConditionEnum.STATUS,
			value: statusList.FLIPPERS
		},
		map: MapZone.JUNGLE,
		gather: GatherType.HUNT
	},
	PORTE_DE_SYLVENOIRE: {
		placeId: 20,
		name: 'garde',
		borderPlace: [19, 21],
		map: MapZone.JUNGLE,
		gather: GatherType.ENERGY1
	},
	GO_TO_STEPPES: {
		placeId: 21,
		name: 'gostep',
		borderPlace: [20],
		conditions: {
			conditionType: ConditionEnum.STATUS,
			value: statusList.SYLVENOIRE_KEY
		},
		alias: 55,
		map: MapZone.STEPPE
	},
	GO_TO_GORGES_PROFONDES: {
		placeId: 22,
		name: 'goorg',
		borderPlace: [18],
		alias: 49,
		map: MapZone.GTOUTCHAUD
	},
	GO_TO_CHUTES: {
		placeId: 23,
		name: 'gochut',
		borderPlace: [14],
		alias: 25,
		map: MapZone.ILES
	},
	GO_TO_FOREST: {
		placeId: 24,
		name: 'gogrum',
		borderPlace: [25],
		conditions: {
			conditionType: ConditionEnum.STATUS,
			value: statusList.NENUPHAR_LEAF
		},
		alias: 14,
		map: MapZone.JUNGLE
	},
	CHUTES_MUTANTES: {
		placeId: 25,
		name: 'chutes',
		borderPlace: [24, 26, 27, 29],
		map: MapZone.ILES,
		gather: GatherType.FISH
	},
	GO_TO_DOME_SOULAFLOTTE: {
		placeId: 26,
		name: 'rasca',
		borderPlace: [25],
		alias: 28,
		conditions: {
			conditionType: ConditionEnum.STATUS,
			value: statusList.RASCAPHANDRE_DECOY
		},
		map: MapZone.ILES
	},
	BAO_BOB: {
		placeId: 27,
		name: 'baobob',
		borderPlace: [25],
		map: MapZone.ILES,
		gather: GatherType.HUNT
	},
	DOME_SOULAFLOTTE: {
		placeId: 28,
		name: 'dome',
		borderPlace: [25],
		conditions: {
			conditionType: ConditionEnum.STATUS,
			value: statusList.RASCAPHANDRE_DECOY
		},
		map: MapZone.ILES,
		gather: GatherType.ENERGY1
	},
	MARAIS_COLLANT: {
		placeId: 29,
		name: 'marais',
		borderPlace: [25, 30, 31, 33],
		map: MapZone.ILES,
		gather: GatherType.CUEILLE1
	},
	MINES_DE_CORAIL: {
		placeId: 30,
		name: 'corail',
		borderPlace: [29, 31],
		map: MapZone.ILES,
		gather: GatherType.SEEK
	},
	ILE_WAIKIKI: {
		placeId: 31,
		name: 'ilewkk',
		borderPlace: [29, 30, 32],
		map: MapZone.ILES,
		gather: GatherType.FISH
	},
	GO_TO_PORT_DE_PRECHE: {
		placeId: 32,
		name: 'goport',
		borderPlace: [31],
		alias: 1,
		map: MapZone.DINOLAND
	},
	ATELIER_BROC: {
		placeId: 33,
		name: 'chbroc',
		borderPlace: [29],
		map: MapZone.ILES
	},
	GO_TO_DINOVILLE: {
		placeId: 34,
		name: 'godnv',
		borderPlace: [35],
		alias: 5,
		map: MapZone.DINOLAND
	},
	DINOPLAZA: {
		placeId: 35,
		name: 'dplaza',
		borderPlace: [36, 37, 38, 34],
		map: MapZone.DINOWEST
	},
	VILLA: {
		placeId: 36,
		name: 'villa',
		borderPlace: [35, 37],
		map: MapZone.DINOWEST
	},
	CINEMA_PARADINO: {
		placeId: 37,
		name: 'dcine',
		borderPlace: [36, 35, 38, 40],
		map: MapZone.DINOWEST
	},
	CLINIQUE: {
		placeId: 38,
		name: 'clinik',
		borderPlace: [35, 37],
		map: MapZone.DINOWEST
	},
	CHATEAU_DE_DINOVILLE: {
		placeId: 39,
		name: 'chato',
		borderPlace: [40],
		map: MapZone.DINOWEST
	},
	POSTE_DE_GARDE: {
		placeId: 40,
		name: 'poste',
		borderPlace: [39, 41, 37],
		map: MapZone.DINOWEST
	},
	GO_TO_VOIE_TEMPLE_CELESTE: {
		placeId: 41,
		name: 'portal',
		borderPlace: [40],
		map: MapZone.DINOWEST
	},
	GO_TO_COLLINES_ESCARPEES: {
		placeId: 42,
		name: 'gocol',
		borderPlace: [43],
		alias: 8,
		map: MapZone.DINOLAND
	},
	PENTES_DE_BASALTE: {
		placeId: 43,
		name: 'bslt',
		borderPlace: [42, 44],
		map: MapZone.GTOUTCHAUD,
		gather: GatherType.ENERGY1
	},
	FORGES_DU_GTC: {
		placeId: 44,
		name: 'forges',
		borderPlace: [43, 45, 46, 47],
		map: MapZone.GTOUTCHAUD,
		gather: GatherType.CUEILLE1
	},
	RUINES_ASHPOUK: {
		placeId: 45,
		name: 'rashpk',
		borderPlace: [44],
		map: MapZone.GTOUTCHAUD,
		gather: GatherType.SEEK
	},
	FOSSELAVE: {
		placeId: 46,
		name: 'fosslv',
		borderPlace: [44, 48],
		map: MapZone.GTOUTCHAUD,
		gather: GatherType.SEEK
	},
	REPAIRE_DU_VENERABLE: {
		placeId: 47,
		name: 'vener',
		borderPlace: [44, 52],
		map: MapZone.GTOUTCHAUD,
		gather: GatherType.HUNT
	},
	TUNNEL_SOUS_LA_BRANCHE: {
		placeId: 48,
		name: 'tunel',
		borderPlace: [46, 50],
		map: MapZone.GTOUTCHAUD
	},
	GORGES_PROFONDES: {
		placeId: 49,
		name: 'gorges',
		borderPlace: [48, 51],
		map: MapZone.GTOUTCHAUD
	},
	GO_TO_TUNNEL: {
		placeId: 50,
		name: 'stunel',
		borderPlace: [48],
		conditions: {
			conditionType: ConditionEnum.STATUS,
			value: statusList.LANTERN
		},
		alias: 49,
		map: MapZone.GTOUTCHAUD
	},
	GO_TO_CAMP_KORGON: {
		placeId: 51,
		name: 'gocamp',
		borderPlace: [50],
		alias: 18,
		map: MapZone.JUNGLE
	},
	GO_TO_KARINBAO_TOWER: {
		placeId: 52,
		name: 'tourbt',
		borderPlace: [47, 53],
		map: MapZone.GTOUTCHAUD
	},
	GO_TO_CELESTIAL_ISLAND: {
		placeId: 53,
		name: 'toursk',
		borderPlace: [52],
		alias: 81,
		map: MapZone.NIMBAO
	},
	GO_TO_SYLVENOIRE_DOOR: {
		placeId: 54,
		name: 'gosylv',
		borderPlace: [55],
		alias: 20,
		map: MapZone.JUNGLE
	},
	FRONTIERE_CREPITANTE: {
		placeId: 55,
		name: 'senter',
		borderPlace: [54, 56, 57],
		map: MapZone.STEPPE,
		gather: GatherType.CUEILLE1
	},
	CROISEE_DES_NOMADES: {
		placeId: 56,
		name: 'scross',
		borderPlace: [58, 55, 61],
		map: MapZone.STEPPE
	},
	AVANT_POSTE_ROCKY: {
		placeId: 57,
		name: 'svillg',
		borderPlace: [55, 58],
		map: MapZone.STEPPE,
		gather: GatherType.HUNT
	},
	CITADELLE_DU_ROI: {
		placeId: 58,
		name: 'sking',
		borderPlace: [56, 57, 59, 62],
		map: MapZone.STEPPE,
		gather: GatherType.SEEK
	},
	PYLONES_DE_MAGNETITES: {
		placeId: 59,
		name: 'spylon',
		borderPlace: [58, 60, 63],
		map: MapZone.STEPPE,
		gather: GatherType.ENERGY1
	},
	SYPHON_SIFFLEUR: {
		placeId: 60,
		name: 'slake',
		borderPlace: [59, 61, 64, 65, 101],
		map: MapZone.STEPPE,
		gather: GatherType.FISH
	},
	SENTIER_DE_TOUTEMBA: {
		placeId: 61,
		name: 'scanyo',
		borderPlace: [56, 60],
		map: MapZone.STEPPE,
		gather: GatherType.ENERGY1
	},
	DEVOREUSE_DE_L_EST: {
		placeId: 62,
		name: 'stowr1',
		borderPlace: [58],
		map: MapZone.STEPPE
	},
	DEVOREUSE_DU_NORD: {
		placeId: 63,
		name: 'stowr2',
		borderPlace: [59],
		map: MapZone.STEPPE
	},
	DEVOREUSE_DE_L_OUEST: {
		placeId: 64,
		name: 'stowr3',
		borderPlace: [60],
		map: MapZone.STEPPE
	},
	TAUDIS_DES_ZAXA: {
		placeId: 65,
		name: 'sband1',
		borderPlace: [60, 66, 67],
		map: MapZone.STEPPE
	},
	CAMP_DES_EMMEMMA: {
		placeId: 66,
		name: 'sband2',
		borderPlace: [65, 67, 70],
		map: MapZone.STEPPE
	},
	CAMPEMENT_DES_MATTMUT: {
		placeId: 67,
		name: 'sband3',
		borderPlace: [65, 66, 68],
		map: MapZone.STEPPE
	},
	REPAIRE_DE_LA_TEAM_W: {
		placeId: 68,
		name: 'scampw',
		borderPlace: [67],
		map: MapZone.STEPPE,
		gather: GatherType.SEEK
	},
	CONFINS_DES_STEPPES: {
		placeId: 69,
		name: 'scaush',
		borderPlace: [66, 70, 71],
		map: MapZone.STEPPE,
		gather: GatherType.CUEILLE1
	},
	PORTES_DE_CAUSHEMESH: {
		placeId: 70,
		name: 'sport',
		borderPlace: [66, 69],
		map: MapZone.STEPPE,
		gather: GatherType.CUEILLE1
	},
	APPROCHER_SYPHON: {
		placeId: 71,
		name: 'sinto1',
		borderPlace: [69],
		alias: 60,
		map: MapZone.STEPPE
	},
	TETE_DE_L_ILE: {
		placeId: 72,
		name: 'iroche',
		borderPlace: [73],
		map: MapZone.NIMBAO,
		gather: GatherType.SEEK
	},
	PONT: {
		placeId: 73,
		name: 'ipont',
		borderPlace: [72, 74],
		map: MapZone.NIMBAO
	},
	PORTE_DE_NIVEAU_SUPERIEUR: {
		placeId: 74,
		name: 'iporte',
		borderPlace: [73, 77, 83, 75, 88],
		map: MapZone.NIMBAO
	},
	CITE_ARBORIS: {
		placeId: 75,
		name: 'icite',
		borderPlace: [74, 76, 81, 89],
		map: MapZone.NIMBAO
	},
	LAC_CELESTE: {
		placeId: 76,
		name: 'ilacro',
		borderPlace: [75, 88, 90],
		map: MapZone.NIMBAO,
		gather: GatherType.HUNT
	},
	PLAINES_ENNEIGEES: {
		placeId: 77,
		name: 'iplain',
		borderPlace: [74, 78],
		map: MapZone.NIMBAO
	},
	BOIS_GIVRES: {
		placeId: 78,
		name: 'isnow2',
		borderPlace: [77, 79],
		map: MapZone.NIMBAO,
		gather: GatherType.CUEILLE2
	},
	MONT_SACRE_D_EVEROUEST: {
		placeId: 79,
		name: 'imont',
		borderPlace: [78, 80],
		map: MapZone.NIMBAO
	},
	SOMMET_DU_MONT_SACRE: {
		placeId: 80,
		name: 'ihaut',
		borderPlace: [79],
		map: MapZone.NIMBAO
	},
	CHEMIN_OBSERVATOIRE: {
		placeId: 81,
		name: 'voie',
		borderPlace: [75, 82, 91],
		map: MapZone.NIMBAO //Redescente tout chaud
	},
	OBSERVATOIRE: {
		placeId: 82,
		name: 'observ',
		borderPlace: [81],
		map: MapZone.NIMBAO
	},
	QUARTIER_LUXURIANT: {
		placeId: 83,
		name: 'ville1',
		borderPlace: [74, 84, 87],
		map: MapZone.NIMBAO
	},
	QUARTIER_EXUBERANT: {
		placeId: 84,
		name: 'ville2',
		borderPlace: [83, 85],
		map: MapZone.NIMBAO
	},
	CHEMIN_VERS_PALAIS: {
		placeId: 85,
		name: 'sommet',
		borderPlace: [84, 86, 87],
		map: MapZone.NIMBAO
	},
	PALAIS_DE_L_ARCHIDOROGON: {
		placeId: 86,
		name: 'palais',
		borderPlace: [85],
		map: MapZone.NIMBAO
	},
	EGOUTS_DU_PALAIS: {
		placeId: 87,
		name: 'egout',
		borderPlace: [83, 85],
		map: MapZone.NIMBAO
	},
	CHUTES_DE_NIRVANA: {
		placeId: 88,
		name: 'ilac',
		borderPlace: [74, 76],
		map: MapZone.NIMBAO,
		gather: GatherType.ENERGY2
	},
	PRIRANESE: {
		placeId: 89,
		name: 'prison',
		borderPlace: [75, 90],
		map: MapZone.NIMBAO
	},
	AILE_OUEST_DU_DRAGON: {
		placeId: 90,
		name: 'ilac2',
		borderPlace: [89, 76],
		map: MapZone.NIMBAO
	},
	TOURUP: {
		placeId: 91,
		name: 'tourup',
		borderPlace: [89, 76, 81],
		alias: 52,
		map: MapZone.NIMBAO
	},
	PORT_MONSTRUEUX: {
		placeId: 92,
		name: 'mport',
		borderPlace: [98, 96, 93],
		map: MapZone.ILEMONSTRE
	},
	AVANT_POSTE_FRUTOX: {
		placeId: 93,
		name: 'mfoutp',
		borderPlace: [92, 96, 95, 94],
		map: MapZone.ILEMONSTRE
	},
	PALAIS_DU_GROTOX: {
		placeId: 94,
		name: 'mfpalc',
		borderPlace: [93, 95],
		map: MapZone.ILEMONSTRE
	},
	FORET_KAZE_KAMI: {
		placeId: 95,
		name: 'mforst',
		borderPlace: [93, 94, 99],
		map: MapZone.ILEMONSTRE
	},
	AVANT_POSTE_VEGETOX: {
		placeId: 96,
		name: 'mvoutp',
		borderPlace: [92, 93, 97],
		map: MapZone.ILEMONSTRE
	},
	PALAIX_D_ANTRAXOV: {
		placeId: 97,
		name: 'mvpalc',
		borderPlace: [96, 100],
		map: MapZone.ILEMONSTRE
	},
	GO_TO_PORT_DE_PRECHE_THROUGHT_MONSTER_ISLAND: {
		placeId: 98,
		name: 'bkport',
		borderPlace: [92],
		alias: 1,
		map: MapZone.DINOLAND
	},
	RUINES_DE_CUSCOUZ: {
		placeId: 99,
		name: 'mcuzco',
		borderPlace: [95, 100],
		map: MapZone.ILEMONSTRE
	},
	CAMP_D_ELIT: {
		placeId: 100,
		name: 'mcelit',
		borderPlace: [97, 99],
		map: MapZone.ILEMONSTRE
	},
	APPROCHER_SYPHON2: {
		placeId: 101,
		name: 'sinto2',
		borderPlace: [60],
		alias: 69,
		map: MapZone.STEPPE
	},
	//DarkWorld
	GOUFFRE: {
		placeId: 103,
		name: 'dkchut',
		borderPlace: [104, 114, 111, 114, 115],
		map: MapZone.DARKWORLD
	},
	TOUR_SOMBRE: {
		placeId: 104,
		name: 'dktow',
		borderPlace: [103, 113, 105],
		map: MapZone.DARKWORLD
	},
	TOUR_SOMBRE_1: {
		placeId: 105,
		name: 'dktow2',
		borderPlace: [104, 106],
		conditions: {
			conditionType: ConditionEnum.CURRENT_MISSION,
			value: 'monte',
			operator: ConditionOperatorEnum.OR,
			nextCondition: {
				conditionType: ConditionEnum.CURRENT_MISSION,
				value: 'roif',
				operator: ConditionOperatorEnum.OR,
				nextCondition: {
					conditionType: ConditionEnum.SCENARIO,
					value: 'smog',
					step: 16,
					operator: ConditionOperatorEnum.OR,
					nextCondition: {
						conditionType: ConditionEnum.SCENARIO,
						value: 'smog',
						step: 15
					}
				}
			}
		},
		map: MapZone.DARKWORLD
	},
	TOUR_SOMBRE_2: {
		placeId: 106,
		name: 'dktow3',
		borderPlace: [105, 107],
		map: MapZone.DARKWORLD
	},
	TOUR_SOMBRE_DONJON_1: {
		placeId: 107,
		name: 'dktow4',
		borderPlace: [106, 108, 109],
		map: MapZone.DARKWORLD
	},
	TOUR_SOMBRE_DONJON_2: {
		placeId: 108,
		name: 'dktowa',
		borderPlace: [107, 110],
		map: MapZone.DARKWORLD
	},
	TOUR_SOMBRE_DONJON_3: {
		placeId: 109,
		name: 'dktowb',
		borderPlace: [107, 110],
		map: MapZone.DARKWORLD
	},
	TOUR_SOMBRE_DONJON_LAST: {
		placeId: 110,
		name: 'dktows',
		borderPlace: [108, 109],
		map: MapZone.DARKWORLD
	},
	DARK_FAKE: {
		placeId: 111,
		name: 'fake',
		borderPlace: [103],
		conditions: {
			conditionType: ConditionEnum.CURRENT_MISSION,
			value: 'ouestu',
			operator: ConditionOperatorEnum.OR,
			nextCondition: {
				conditionType: ConditionEnum.CURRENT_MISSION,
				value: 'lumi',
				operator: ConditionOperatorEnum.OR,
				nextCondition: {
					conditionType: ConditionEnum.CURRENT_MISSION,
					value: 'truci2',
					operator: ConditionOperatorEnum.OR,
					nextCondition: {
						conditionType: ConditionEnum.STATUS,
						value: statusList.DARK_ORB
					}
				}
			}
		},
		map: MapZone.DARKWORLD
	},
	DARK_FAKE_2: {
		placeId: 112,
		name: 'fake2',
		borderPlace: [102],
		conditions: {
			conditionType: ConditionEnum.CURRENT_MISSION,
			value: 'roid'
		},
		map: MapZone.DARKWORLD
	},
	TOUR_SOMBRE_ENTREE: {
		placeId: 113,
		name: 'gotow',
		borderPlace: [104],
		conditions: {
			conditionType: ConditionEnum.CURRENT_MISSION,
			value: 'monte',
			reverse: true,
			operator: ConditionOperatorEnum.OR,
			nextCondition: {
				conditionType: ConditionEnum.CURRENT_MISSION,
				value: 'roif',
				reverse: true,
				operator: ConditionOperatorEnum.OR,
				nextCondition: {
					conditionType: ConditionEnum.SCENARIO,
					value: 'smog',
					step: 16,
					reverse: true
				}
			}
		},
		alias: 105,
		map: MapZone.DARKWORLD
	},
	RETOUR_SURFACE: {
		placeId: 114,
		name: 'rechut',
		conditions: {
			conditionType: ConditionEnum.ACTIVE,
			value: ''
		},
		borderPlace: [],
		alias: 25,
		map: MapZone.DARKWORLD
	},
	PORTAIL: {
		placeId: 115,
		name: 'dkbao',
		borderPlace: [103, 112],
		map: MapZone.DARKWORLD
	},
	NOWHERE: {
		placeId: 999,
		name: 'nowhere',
		borderPlace: [],
		map: MapZone.ALL
	}
};
