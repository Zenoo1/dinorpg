import { IngredientFiche } from '@drpg/core/models/ingredient/IngredientFiche';

export const ingredientList: Readonly<Record<string, IngredientFiche>> = {
	MEROU_LUJIDANE: {
		ingredientId: 1,
		maxQuantity: 60 // TODO : Valeur à vérifier
	},
	POISSON_VENGEUR: {
		ingredientId: 2,
		maxQuantity: 24 // TODO : Valeur à vérifier
	},
	AN_GUILI_GUILILLE: {
		ingredientId: 3,
		maxQuantity: 6 // TODO : Valeur à vérifier
	},
	GLOBULOS: {
		ingredientId: 4,
		maxQuantity: 6 // TODO : Valeur à vérifier
	},
	SUPER_POISSON: {
		ingredientId: 5,
		maxQuantity: 6 // TODO : Valeur à vérifier
	},
	TOUFFE_DE_FOURRURE: {
		ingredientId: 6,
		maxQuantity: 60 // TODO : Valeur à vérifier
	},
	ROCHE_RADIO_ACTIVE: {
		ingredientId: 7,
		maxQuantity: 24 // TODO : Valeur à vérifier
	},
	GRIFFES_ACEREES: {
		ingredientId: 8,
		maxQuantity: 24 // TODO : Valeur à vérifier
	},
	CORNE_EN_CHOCOLAT: {
		ingredientId: 9,
		maxQuantity: 6 // TODO : Valeur à vérifier
	},
	OEIL_VISQUEUX: {
		ingredientId: 10,
		maxQuantity: 6 // TODO : Valeur à vérifier
	},
	LANGUE_MONSTRUEUSE: {
		ingredientId: 11,
		maxQuantity: 10 // TODO : Valeur à vérifier
	},
	ENERGIE_FOUDRE: {
		ingredientId: 12,
		maxQuantity: 60 // TODO : Valeur à vérifier
	},
	ENERGIE_AIR: {
		ingredientId: 13,
		maxQuantity: 24 // TODO : Valeur à vérifier
	},
	ENERGIE_EAU: {
		ingredientId: 14,
		maxQuantity: 6 // TODO : Valeur à vérifier
	},
	ENERGIE_FEU: {
		ingredientId: 15,
		maxQuantity: 6 // TODO : Valeur à vérifier
	},
	ENERGIE_BOIS: {
		ingredientId: 16,
		maxQuantity: 6 // TODO : Valeur à vérifier
	},
	SILEX_TAILLE: {
		ingredientId: 17,
		maxQuantity: 60 // TODO : Valeur à vérifier
	},
	FRAGMENT_DE_TEXTE_ANCIEN: {
		ingredientId: 18,
		maxQuantity: 24 // TODO : Valeur à vérifier
	},
	VIEIL_ANNEAU_PRECIEUX: {
		ingredientId: 19,
		maxQuantity: 6 // TODO : Valeur à vérifier
	},
	CALICE_CISELE: {
		ingredientId: 20,
		maxQuantity: 6 // TODO : Valeur à vérifier
	},
	COLLIER_KARAT: {
		ingredientId: 21,
		maxQuantity: 6 // TODO : Valeur à vérifier
	},
	BROCHE_EN_PARFAIT_ETAT: {
		ingredientId: 22,
		maxQuantity: 6 // TODO : Valeur à vérifier
	},
	SUPERBE_COURONNE_ROYALE: {
		ingredientId: 23,
		maxQuantity: 6 // TODO : Valeur à vérifier
	},
	FEUILLES_DE_PELINAE: {
		ingredientId: 24,
		maxQuantity: 60 // TODO : Valeur à vérifier
	},
	BOLET_PHALISK_BLANC: {
		ingredientId: 25,
		maxQuantity: 24 // TODO : Valeur à vérifier
	},
	ORCHIDEE_FANTASQUE: {
		ingredientId: 26,
		maxQuantity: 6 // TODO : Valeur à vérifier
	},
	RACINE_DE_FIGONICIA: {
		ingredientId: 27,
		maxQuantity: 10 // TODO : Valeur à vérifier
	},
	SADIQUAE_MORDICUS: {
		ingredientId: 28,
		maxQuantity: 6 // TODO : Valeur à vérifier
	},
	FLAUREOLE: {
		ingredientId: 29,
		maxQuantity: 6 // TODO : Valeur à vérifier
	},
	SPORE_ETHERAL: {
		ingredientId: 30,
		maxQuantity: 24 // TODO : Valeur à vérifier
	},
	POUSSE_SOMBRE: {
		ingredientId: 31,
		maxQuantity: 24 // TODO : Valeur à vérifier
	},
	GRAINE_DE_DEVOREUSE: {
		ingredientId: 32,
		maxQuantity: 120 // TODO : Valeur à vérifier
	},
	DENT_DE_DOROGON: {
		ingredientId: 33,
		maxQuantity: 10 // TODO : Valeur à vérifier
	},
	BRAS_MECANIQUE: {
		ingredientId: 34,
		maxQuantity: 10 // TODO : Valeur à vérifier
	}
};
