import { DinozSkillFiche } from '@drpg/core/models/dinoz/DinozSkillFiche';
import { SkillType } from '@drpg/core/models/enums/SkillType';
import { Energy } from '@drpg/core/models/enums/Energy';
import { ElementType } from '@drpg/core/models/enums/ElementType';
import { SkillTree } from '@drpg/core/models/enums/SkillTree';
import { SkillEffect } from '@drpg/core/models/enums/SkillEffect';
import { raceList } from './race.js';

// skillId are counted like this : ABCDE
// A = Element (from fire to void)
// B = Tree (Vanilla or Ether)
// C = Column of the skill
// DE = Number of the skill in this column

export const skillList: Readonly<Record<string, DinozSkillFiche>> = {
	// Fire Skills
	GRIFFES_ENFLAMMEES: {
		skillId: 11101,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [],
		isBaseSkill: false,
		isSphereSkill: false
	},
	COLERE: {
		skillId: 11102,
		type: SkillType.E,
		energy: Energy.WEAK,
		element: [ElementType.FIRE],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [],
		isBaseSkill: false,
		isSphereSkill: false
	},
	FORCE: {
		skillId: 11103,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [],
		isBaseSkill: false,
		isSphereSkill: false
	},
	BRASERO: {
		skillId: 11104,
		type: SkillType.E,
		energy: Energy.NORMAL,
		element: [ElementType.FIRE],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [],
		isBaseSkill: false,
		isSphereSkill: true
	},
	SOUFFLE_ARDENT: {
		skillId: 11201,
		type: SkillType.A,
		energy: Energy.WEAK,
		element: [ElementType.FIRE],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11101],
		isBaseSkill: false,
		isSphereSkill: false
	},
	CHARGE: {
		skillId: 11202,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11103],
		isBaseSkill: false,
		isSphereSkill: false
	},
	SANG_CHAUD: {
		skillId: 11203,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11102],
		isBaseSkill: false,
		isSphereSkill: false
	},
	FURIE: {
		skillId: 11204,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11102],
		isBaseSkill: false,
		isSphereSkill: false
	},
	CHASSEUR_DE_GOUPIGNON: {
		skillId: 11205,
		type: SkillType.C,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11101],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ARTS_MARTIAUX: {
		skillId: 11206,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11103],
		isBaseSkill: false,
		isSphereSkill: false
	},
	DETONATION: {
		skillId: 11207,
		type: SkillType.E,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11104],
		isBaseSkill: false,
		isSphereSkill: true
	},
	PROPULSION_DIVINE: {
		skillId: 11208,
		type: SkillType.P,
		energy: Energy.WEAK,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11103],
		raceId: [raceList.QUETZU.raceId],
		isBaseSkill: false,
		isSphereSkill: false,
		effects: [
			{
				type: SkillEffect.CHANGE_ELEMENT,
				value: -2,
				element: ElementType.FIRE
			}
		]
	},
	VIGILANCE: {
		skillId: 11301,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11206],
		isBaseSkill: false,
		isSphereSkill: false
	},
	COEUR_ARDENT: {
		skillId: 11302,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11203],
		isBaseSkill: false,
		isSphereSkill: false,
		effects: [
			{
				type: SkillEffect.CHANGE_MAX_LIFE,
				value: 20
			}
		]
	},
	COULEE_DE_LAVE: {
		skillId: 11303,
		type: SkillType.A,
		energy: Energy.NORMAL,
		element: [ElementType.FIRE],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11205],
		isBaseSkill: false,
		isSphereSkill: false
	},
	SIESTE: {
		skillId: 11304,
		type: SkillType.A,
		energy: Energy.WEAK,
		element: [ElementType.FIRE],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11203],
		isBaseSkill: false,
		isSphereSkill: false
	},
	KAMIKAZE: {
		skillId: 11305,
		type: SkillType.A,
		energy: Energy.WEAK,
		element: [ElementType.FIRE],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11202],
		isBaseSkill: false,
		isSphereSkill: false
	},
	CHASSEUR_DE_GEANT: {
		skillId: 11306,
		type: SkillType.C,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11205],
		isBaseSkill: false,
		isSphereSkill: false
	},
	BOULE_DE_FEU: {
		skillId: 11307,
		type: SkillType.A,
		energy: Energy.WEAK,
		element: [ElementType.FIRE],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11201],
		isBaseSkill: false,
		isSphereSkill: false
	},
	WAIKIKIDO: {
		skillId: 11308,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11206],
		isBaseSkill: false,
		isSphereSkill: false,
		effects: [
			{
				type: SkillEffect.CHANGE_MAX_LIFE,
				value: 20
			}
		]
	},
	AURA_INCANDESCENTE: {
		skillId: 11309,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11204],
		isBaseSkill: false,
		isSphereSkill: false,
		effects: [
			{
				type: SkillEffect.CHANGE_ELEMENT,
				value: 2,
				element: ElementType.FIRE
			}
		]
	},
	VENGEANCE: {
		skillId: 11310,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11204],
		isBaseSkill: false,
		isSphereSkill: false
	},
	COMBUSTION: {
		skillId: 11311,
		type: SkillType.E,
		energy: Energy.WEAK,
		element: [ElementType.FIRE],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11201],
		isBaseSkill: false,
		isSphereSkill: false
	},
	PAUME_CHALUMEAU: {
		skillId: 11312,
		type: SkillType.A,
		energy: Energy.WEAK,
		element: [ElementType.FIRE],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11206],
		isBaseSkill: false,
		isSphereSkill: false
	},
	COEUR_DU_PHOENIX: {
		skillId: 11313,
		type: SkillType.P,
		energy: Energy.VHIGH,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11207],
		isBaseSkill: false,
		isSphereSkill: true
	},
	BOUDDHA: {
		skillId: 11314,
		type: SkillType.I,
		energy: Energy.VHIGH,
		element: [ElementType.FIRE, ElementType.LIGHTNING, ElementType.AIR],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41201, 11206, 51309, 61121],
		raceId: [raceList.HIPPOCLAMP.raceId],
		isBaseSkill: false,
		isSphereSkill: false
	},
	GRIFFES_INFERNALES: {
		skillId: 11315,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11208],
		raceId: [raceList.QUETZU.raceId],
		isBaseSkill: false,
		isSphereSkill: false
	},
	CHASSEUR_DE_DRAGON: {
		skillId: 11401,
		type: SkillType.C,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11306],
		isBaseSkill: false,
		isSphereSkill: false
	},
	BELIER: {
		skillId: 11402,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11305],
		isBaseSkill: false,
		isSphereSkill: false
	},
	TORCHE: {
		skillId: 11403,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11311],
		isBaseSkill: false,
		isSphereSkill: false
	},
	SELF_CONTROL: {
		skillId: 11404,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11304],
		isBaseSkill: false,
		isSphereSkill: false
	},
	SPRINT: {
		skillId: 11405,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.FIRE, ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11305, 41303, 61119],
		isBaseSkill: false,
		isSphereSkill: false
	},
	VENDETTA: {
		skillId: 11406,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.FIRE, ElementType.AIR],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11310, 51306, 61119],
		isBaseSkill: false,
		isSphereSkill: false
	},
	METEORES: {
		skillId: 11407,
		type: SkillType.A,
		energy: Energy.HIGH,
		element: [ElementType.FIRE],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11309],
		isBaseSkill: false,
		isSphereSkill: false
	},
	CHEF_DE_GUERRE: {
		skillId: 11408,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11301],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ARMURE_DE_BASALTE: {
		skillId: 11409,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.FIRE, ElementType.WOOD],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11308, 21303, 61119],
		isBaseSkill: false,
		isSphereSkill: false
	},
	MAITRE_ELEMENTAIRE: {
		skillId: 11410,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.FIRE, ElementType.WATER],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11311, 31301, 61119],
		isBaseSkill: false,
		isSphereSkill: false,
		effects: [
			{
				type: SkillEffect.CHANGE_ELEMENT,
				value: 2,
				element: ElementType.WATER
			},
			{
				type: SkillEffect.CHANGE_ELEMENT,
				value: 2,
				element: ElementType.FIRE
			}
		]
	},
	SALAMANDRE: {
		skillId: 11411,
		type: SkillType.I,
		energy: Energy.VHIGH,
		element: [ElementType.FIRE, ElementType.WATER],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31305, 11312, 61121],
		raceId: [raceList.FEROSS.raceId],
		isBaseSkill: false,
		isSphereSkill: false
	},
	VULCAIN: {
		skillId: 11412,
		type: SkillType.I,
		energy: Energy.VHIGH,
		element: [ElementType.FIRE, ElementType.LIGHTNING],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11303, 41312, 61121],
		raceId: [raceList.MOUEFFE.raceId, raceList.MOUEFFE_DEMON.raceId],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ARMURE_DIFRIT: {
		skillId: 11413,
		type: SkillType.I,
		energy: Energy.VHIGH,
		element: [ElementType.FIRE, ElementType.WOOD],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11307, 21311, 61121],
		raceId: [raceList.PIGMOU.raceId, raceList.PIGMOU_DEMON.raceId],
		isBaseSkill: false,
		isSphereSkill: false
	},
	BRAVE: {
		skillId: 11501,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11404],
		isBaseSkill: false,
		isSphereSkill: false,
		effects: [
			{
				type: SkillEffect.CHANGE_MAX_LIFE,
				value: 50
			},
			{
				type: SkillEffect.CHANGE_ELEMENT,
				value: 6,
				element: ElementType.FIRE
			}
		]
	},
	PROTEINES_DINOZIENNES: {
		skillId: 12101,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [],
		isBaseSkill: false,
		isSphereSkill: false
	},
	EXTENUATION: {
		skillId: 12201,
		type: SkillType.A,
		energy: Energy.WEAK,
		element: [ElementType.FIRE],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [12101],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ROUGE: {
		skillId: 12202,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [12101],
		isBaseSkill: false,
		isSphereSkill: false
	},
	CARAPACE_DE_MAGMA: {
		skillId: 12301,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [12201],
		isBaseSkill: false,
		isSphereSkill: false,
		effects: [
			{
				type: SkillEffect.CHANGE_MAX_LIFE,
				value: 30
			}
		]
	},
	CRI_DE_GUERRE: {
		skillId: 12302,
		type: SkillType.E,
		energy: Energy.HIGH,
		element: [ElementType.FIRE],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [12202],
		isBaseSkill: false,
		isSphereSkill: false
	},
	FIEVRE_BRULANTE: {
		skillId: 12401,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [12301],
		isBaseSkill: false,
		isSphereSkill: false
	},
	BENEDICTION_DARTEMIS: {
		skillId: 12402,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [12301],
		isBaseSkill: false,
		isSphereSkill: false
	},
	JOKER: {
		skillId: 12403,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [12302],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ARMURE_DE_FEU: {
		skillId: 12404,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [12302],
		isBaseSkill: false,
		isSphereSkill: false
	},
	PAYS_DE_CENDRE: {
		skillId: 12501,
		type: SkillType.E,
		energy: Energy.HIGH,
		element: [ElementType.FIRE],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [12401],
		isBaseSkill: false,
		isSphereSkill: false
	},
	RECEPTACLE_ROCHEUX: {
		skillId: 12502,
		type: SkillType.A,
		energy: Energy.HIGH,
		element: [ElementType.FIRE],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [12402],
		isBaseSkill: false,
		isSphereSkill: false
	},
	PLUMES_DE_PHOENIX: {
		skillId: 12503,
		type: SkillType.E,
		energy: Energy.WEAK,
		element: [ElementType.FIRE],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [12402],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ACCLAMATION_FRATERNELLE: {
		skillId: 12504,
		type: SkillType.E,
		energy: Energy.NORMAL,
		element: [ElementType.FIRE],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [12403],
		isBaseSkill: false,
		isSphereSkill: false
	},
	POING_DE_FEU: {
		skillId: 12505,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.FIRE],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [12404],
		isBaseSkill: false,
		isSphereSkill: false
	},
	// WOOD Skills
	CARAPACE: {
		skillId: 21101,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [],
		isBaseSkill: false,
		isSphereSkill: false
	},
	SAUVAGERIE: {
		skillId: 21102,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ENDURANCE: {
		skillId: 21103,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [],
		isBaseSkill: false,
		isSphereSkill: false
	},
	LANCEUR_DE_GLAND: {
		skillId: 21104,
		type: SkillType.A,
		energy: Energy.NORMAL,
		element: [ElementType.WOOD],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [],
		isBaseSkill: false,
		isSphereSkill: true
	},
	VIGNES: {
		skillId: 21201,
		type: SkillType.E,
		energy: Energy.WEAK,
		element: [ElementType.WOOD],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21101],
		isBaseSkill: false,
		isSphereSkill: false
	},
	RENFORTS_KORGON: {
		skillId: 21202,
		type: SkillType.E,
		energy: Energy.NORMAL,
		element: [ElementType.WOOD],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21102],
		isBaseSkill: false,
		isSphereSkill: false
	},
	SYMPATIQUE: {
		skillId: 21203,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21101],
		isBaseSkill: false,
		isSphereSkill: false
	},
	TENACITE: {
		skillId: 21204,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21102],
		isBaseSkill: false,
		isSphereSkill: false
	},
	FOUILLE: {
		skillId: 21205,
		type: SkillType.C,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21103],
		isBaseSkill: false,
		isSphereSkill: false
	},
	CROISSANCE: {
		skillId: 21206,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21103],
		isBaseSkill: false,
		isSphereSkill: false,
		effects: [
			{
				type: SkillEffect.CHANGE_MAX_LIFE,
				value: 20
			}
		]
	},
	GRATTEUR: {
		skillId: 21207,
		type: SkillType.C,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21104],
		isBaseSkill: false,
		isSphereSkill: true
	},
	ETAT_PRIMAL: {
		skillId: 21301,
		type: SkillType.E,
		energy: Energy.WEAK,
		element: [ElementType.WOOD],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21201],
		isBaseSkill: false,
		isSphereSkill: false
	},
	DETECTIVE: {
		skillId: 21302,
		type: SkillType.C,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21205],
		isBaseSkill: false,
		isSphereSkill: false
	},
	COCON: {
		skillId: 21303,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21206],
		isBaseSkill: false,
		isSphereSkill: false
	},
	INSTINCT_SAUVAGE: {
		skillId: 21304,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21202],
		isBaseSkill: false,
		isSphereSkill: false,
		effects: [
			{
				type: SkillEffect.CHANGE_ELEMENT,
				value: 2,
				element: ElementType.WOOD
			}
		]
	},
	LARGE_MACHOIRE: {
		skillId: 21305,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21206],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ACROBATE: {
		skillId: 21306,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21202],
		isBaseSkill: false,
		isSphereSkill: false
	},
	PRINTEMPS_PRECOCE: {
		skillId: 21307,
		type: SkillType.E,
		energy: Energy.WEAK,
		element: [ElementType.WOOD],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21201],
		isBaseSkill: false,
		isSphereSkill: false
	},
	CHARISME: {
		skillId: 21308,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21204],
		isBaseSkill: false,
		isSphereSkill: false
	},
	RESISTANCE_A_LA_MAGIE: {
		skillId: 21309,
		type: SkillType.E,
		energy: Energy.WEAK,
		element: [ElementType.WOOD],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21204],
		isBaseSkill: false,
		isSphereSkill: false
	},
	PLANIFICATEUR: {
		skillId: 21310,
		type: SkillType.C,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21203],
		isBaseSkill: false,
		isSphereSkill: false
	},
	HERITAGE_FAROE: {
		skillId: 21311,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21203],
		isBaseSkill: false,
		isSphereSkill: false
	},
	EXPERT_EN_FOUILLE: {
		skillId: 21312,
		type: SkillType.C,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21205],
		isBaseSkill: false,
		isSphereSkill: false
	},
	GROSSE_BEIGNE: {
		skillId: 21313,
		type: SkillType.E,
		energy: Energy.HIGH,
		element: [ElementType.WOOD],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21207],
		isBaseSkill: false,
		isSphereSkill: true
	},
	ESPRIT_GORILLOZ: {
		skillId: 21401,
		type: SkillType.E,
		energy: Energy.HIGH,
		element: [ElementType.WOOD],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21311],
		isBaseSkill: false,
		isSphereSkill: false
	},
	LEADER: {
		skillId: 21402,
		type: SkillType.U,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21308],
		isBaseSkill: false,
		isSphereSkill: false
	},
	INGENIEUR: {
		skillId: 21403,
		type: SkillType.U,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21304],
		isBaseSkill: false,
		isSphereSkill: false
	},
	GEANT: {
		skillId: 21404,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21303],
		isBaseSkill: false,
		isSphereSkill: false,
		effects: [
			{
				type: SkillEffect.CHANGE_MAX_LIFE,
				value: 30
			}
		]
	},
	GARDE_FORESTIER: {
		skillId: 21405,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21307],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ARCHEOLOGUE: {
		skillId: 21406,
		type: SkillType.C,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21302],
		isBaseSkill: false,
		isSphereSkill: false
	},
	BENEDICTION_DES_FEES: {
		skillId: 21407,
		type: SkillType.I,
		energy: Energy.VHIGH,
		element: [ElementType.WOOD, ElementType.FIRE],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11302, 21301, 61121],
		raceId: [raceList.GORILLOZ.raceId, raceList.GORILLOZ_DEMON.raceId],
		isBaseSkill: false,
		isSphereSkill: false
	},
	CHOC: {
		skillId: 21408,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WOOD, ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21301, 41306, 61119],
		isBaseSkill: false,
		isSphereSkill: false
	},
	LOUP_GAROU: {
		skillId: 21409,
		type: SkillType.I,
		energy: Energy.VHIGH,
		element: [ElementType.WOOD, ElementType.AIR],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21304, 51311, 61121],
		raceId: [raceList.CASTIVORE.raceId],
		isBaseSkill: false,
		isSphereSkill: false
	},
	COLOSSE: {
		skillId: 21501,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21404],
		isBaseSkill: false,
		isSphereSkill: false,
		effects: [
			{
				type: SkillEffect.CHANGE_MAX_LIFE,
				value: 50
			}
		]
	},
	OXYGENATION_MUSCULAIRE: {
		skillId: 22101,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [],
		isBaseSkill: false,
		isSphereSkill: false
	},
	VERT: {
		skillId: 22102,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [],
		isBaseSkill: false,
		isSphereSkill: false
	},
	SOURCE_DE_VIE: {
		skillId: 22201,
		type: SkillType.S,
		energy: Energy.WEAK,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [22101],
		isBaseSkill: false,
		isSphereSkill: false
	},
	VIDE_ENERGETIQUE: {
		skillId: 22202,
		type: SkillType.S,
		energy: Energy.WEAK,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [22101],
		isBaseSkill: false,
		isSphereSkill: false
	},
	BOUCLIER_DINOZ: {
		skillId: 22203,
		type: SkillType.E,
		energy: Energy.WEAK,
		element: [ElementType.WOOD],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [22102],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ACIDE_LACTIQUE: {
		skillId: 22204,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [22102],
		isBaseSkill: false,
		isSphereSkill: false
	},
	LANCER_DE_ROCHE: {
		skillId: 22301,
		type: SkillType.A,
		energy: Energy.HIGH,
		element: [ElementType.WOOD],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [22201],
		isBaseSkill: false,
		isSphereSkill: false
	},
	COURBATURES: {
		skillId: 22302,
		type: SkillType.E,
		energy: Energy.WEAK,
		element: [ElementType.WOOD],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [22202],
		isBaseSkill: false,
		isSphereSkill: false
	},
	FORCE_CONTROL: {
		skillId: 22303,
		type: SkillType.E,
		energy: Energy.WEAK,
		element: [ElementType.WOOD],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [22203],
		isBaseSkill: false,
		isSphereSkill: false
	},
	PEAU_DE_FER: {
		skillId: 22304,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [22204],
		isBaseSkill: false,
		isSphereSkill: false,
		effects: [
			{
				type: SkillEffect.CHANGE_MAX_LIFE,
				value: 50
			}
		]
	},
	CHAMPOLLION: {
		skillId: 22401,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [22301],
		isBaseSkill: false,
		isSphereSkill: false
	},
	COURANT_DE_VIE: {
		skillId: 22402,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [22302],
		isBaseSkill: false,
		isSphereSkill: false
	},
	BERSERK: {
		skillId: 22403,
		type: SkillType.E,
		energy: Energy.WEAK,
		element: [ElementType.WOOD],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [22303],
		isBaseSkill: false,
		isSphereSkill: false
	},
	RIVIERE_DE_VIE: {
		skillId: 22404,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [22304],
		isBaseSkill: false,
		isSphereSkill: false
	},
	MUR_DE_BOUE: {
		skillId: 22501,
		type: SkillType.A,
		energy: Energy.HIGH,
		element: [ElementType.WOOD],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [22401],
		isBaseSkill: false,
		isSphereSkill: false
	},
	PEAU_DACIER: {
		skillId: 22502,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WOOD],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [22401],
		isBaseSkill: false,
		isSphereSkill: false,
		effects: [
			{
				type: SkillEffect.CHANGE_MAX_LIFE,
				value: 100
			}
		]
	},
	SHARIGNAN: {
		skillId: 22503,
		type: SkillType.E,
		energy: Energy.HIGH,
		element: [ElementType.WOOD],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [22402],
		isBaseSkill: false,
		isSphereSkill: false
	},
	AMAZONIE: {
		skillId: 22504,
		type: SkillType.E,
		energy: Energy.HIGH,
		element: [ElementType.WOOD],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [22403],
		isBaseSkill: false,
		isSphereSkill: false
	},
	RECEPTACLE_AQUEUX: {
		skillId: 22505,
		type: SkillType.A,
		energy: Energy.VHIGH,
		element: [ElementType.WOOD],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [22404],
		isBaseSkill: false,
		isSphereSkill: false
	},
	// Water Skills
	CANON_A_EAU: {
		skillId: 31101,
		type: SkillType.A,
		energy: Energy.WEAK,
		element: [ElementType.WATER],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [],
		isBaseSkill: false,
		isSphereSkill: false
	},
	PERCEPTION: {
		skillId: 31102,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [],
		isBaseSkill: false,
		isSphereSkill: false
	},
	MUTATION: {
		skillId: 31103,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [],
		isBaseSkill: false,
		isSphereSkill: false,
		effects: [
			{
				type: SkillEffect.CHANGE_MAX_LIFE,
				value: 30
			}
		]
	},
	VITALITE: {
		skillId: 31104,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [],
		isBaseSkill: false,
		isSphereSkill: true,
		effects: [
			{
				type: SkillEffect.CHANGE_MAX_LIFE,
				value: 10
			}
		]
	},
	GEL: {
		skillId: 31201,
		type: SkillType.A,
		energy: Energy.WEAK,
		element: [ElementType.WATER],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31101],
		isBaseSkill: false,
		isSphereSkill: false
	},
	DOUCHE_ECOSSAISE: {
		skillId: 31202,
		type: SkillType.A,
		energy: Energy.WEAK,
		element: [ElementType.WATER],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31101],
		isBaseSkill: false,
		isSphereSkill: false
	},
	COUP_SOURNOIS: {
		skillId: 31203,
		type: SkillType.A,
		energy: Energy.WEAK,
		element: [ElementType.WATER],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31102],
		isBaseSkill: false,
		isSphereSkill: false
	},
	APPRENTI_PECHEUR: {
		skillId: 31204,
		type: SkillType.C,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31102],
		isBaseSkill: false,
		isSphereSkill: false
	},
	POCHE_VENTRALE: {
		skillId: 31205,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31103],
		isBaseSkill: false,
		isSphereSkill: false
	},
	KARATE_SOUS_MARIN: {
		skillId: 31206,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31103],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ECAILLES_LUMINESCENTES: {
		skillId: 31207,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31103],
		raceId: [raceList.QUETZU.raceId],
		isBaseSkill: false,
		isSphereSkill: false
	},
	MOIGNONS_LIQUIDES: {
		skillId: 31208,
		type: SkillType.A,
		energy: Energy.VHIGH,
		element: [ElementType.WATER],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31104],
		isBaseSkill: false,
		isSphereSkill: true
	},
	ZERO_ABSOLU: {
		skillId: 31301,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31201],
		isBaseSkill: false,
		isSphereSkill: false
	},
	PETRIFICATION: {
		skillId: 31302,
		type: SkillType.A,
		energy: Energy.WEAK,
		element: [ElementType.WATER],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31201],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ACUPUNCTURE: {
		skillId: 31303,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31202],
		isBaseSkill: false,
		isSphereSkill: false
	},
	SAPEUR: {
		skillId: 31304,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31202],
		isBaseSkill: false,
		isSphereSkill: false
	},
	COUP_FATAL: {
		skillId: 31305,
		type: SkillType.A,
		energy: Energy.WEAK,
		element: [ElementType.WATER],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31203],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ENTRAINEMENT_SOUS_MARIN: {
		skillId: 31306,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31203],
		isBaseSkill: false,
		isSphereSkill: false,
		effects: [
			{
				type: SkillEffect.CHANGE_MAX_LIFE,
				value: 10
			}
		]
	},
	PECHEUR_CONFIRME: {
		skillId: 31307,
		type: SkillType.C,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31204],
		isBaseSkill: false,
		isSphereSkill: false
	},
	MARECAGE: {
		skillId: 31308,
		type: SkillType.E,
		energy: Energy.WEAK,
		element: [ElementType.WATER],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31204],
		isBaseSkill: false,
		isSphereSkill: false
	},
	SUMO: {
		skillId: 31309,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31205],
		isBaseSkill: false,
		isSphereSkill: false,
		effects: [
			{
				type: SkillEffect.CHANGE_MAX_LIFE,
				value: 100
			}
		]
	},
	SANS_PITIE: {
		skillId: 31310,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31205],
		isBaseSkill: false,
		isSphereSkill: false
	},
	CLONE_AQUEUX: {
		skillId: 31311,
		type: SkillType.E,
		energy: Energy.HIGH,
		element: [ElementType.WATER],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31206],
		isBaseSkill: false,
		isSphereSkill: false
	},
	GRIFFES_EMPOISONNEES: {
		skillId: 31312,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31206],
		isBaseSkill: false,
		isSphereSkill: false
	},
	DELUGE: {
		skillId: 31313,
		type: SkillType.A,
		energy: Energy.VHIGH,
		element: [ElementType.WATER],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31208],
		isBaseSkill: false,
		isSphereSkill: true
	},
	PEAU_DE_SERPENT: {
		skillId: 31314,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31207],
		raceId: [raceList.QUETZU.raceId],
		isBaseSkill: false,
		isSphereSkill: false
	},
	RAYON_KAAR_SHER: {
		skillId: 31401,
		type: SkillType.A,
		energy: Energy.HIGH,
		element: [ElementType.WATER],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31302],
		isBaseSkill: false,
		isSphereSkill: false
	},
	MAGASINIER: {
		skillId: 31402,
		type: SkillType.U,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31304],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ENTRAINEMENT_SOUS_MARIN_AVANCE: {
		skillId: 31403,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31306],
		isBaseSkill: false,
		isSphereSkill: false,
		effects: [
			{
				type: SkillEffect.CHANGE_MAX_LIFE,
				value: 20
			}
		]
	},
	MAITRE_PECHEUR: {
		skillId: 31404,
		type: SkillType.C,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31307],
		isBaseSkill: false,
		isSphereSkill: false
	},
	CUISINIER: {
		skillId: 31405,
		type: SkillType.U,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31309],
		isBaseSkill: false,
		isSphereSkill: false
	},
	SANG_ACIDE: {
		skillId: 31406,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31312],
		isBaseSkill: false,
		isSphereSkill: false
	},
	BULLE: {
		skillId: 31407,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WATER, ElementType.AIR],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31304, 51312, 61119],
		isBaseSkill: false,
		isSphereSkill: false
	},
	INCREVABLE: {
		skillId: 31408,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WOOD, ElementType.WATER],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31311, 21309, 61119],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ONDINE: {
		skillId: 31409,
		type: SkillType.I,
		energy: Energy.VHIGH,
		element: [ElementType.WATER, ElementType.LIGHTNING],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31310, 31310, 61121],
		raceId: [raceList.SIRAIN.raceId],
		isBaseSkill: false,
		isSphereSkill: false
	},
	MAITRE_NAGEUR: {
		skillId: 31501,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31403],
		isBaseSkill: false,
		isSphereSkill: false,
		effects: [
			{
				type: SkillEffect.CHANGE_ELEMENT,
				value: 5,
				element: ElementType.WATER
			}
		]
	},
	LEVIATHAN: {
		skillId: 31502,
		type: SkillType.I,
		energy: Energy.VHIGH,
		element: [ElementType.WATER, ElementType.AIR],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [31406, 51206, 61121],
		raceId: [raceList.MAHAMUTI.raceId],
		isBaseSkill: false,
		isSphereSkill: false
	},
	EAU_DIVINE: {
		skillId: 32101,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [],
		isBaseSkill: false,
		isSphereSkill: false
	},
	RADIATIONS_GAMMA: {
		skillId: 32201,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [32101],
		isBaseSkill: false,
		isSphereSkill: false,
		effects: [
			{
				type: SkillEffect.CHANGE_MAX_LIFE,
				value: 30
			}
		]
	},
	BLEU: {
		skillId: 32202,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [32101],
		isBaseSkill: false,
		isSphereSkill: false
	},
	MUE_ACQUEUSE: {
		skillId: 32301,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [32201],
		isBaseSkill: false,
		isSphereSkill: false
	},
	CARAPACE_BLINDEE: {
		skillId: 32302,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [32201],
		isBaseSkill: false,
		isSphereSkill: false
	},
	DIETE_CHROMATIQUE: {
		skillId: 32303,
		type: SkillType.E,
		energy: Energy.HIGH,
		element: [ElementType.WATER],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [32202],
		isBaseSkill: false,
		isSphereSkill: false
	},
	EFFLUVE_APHRODISIAQUE: {
		skillId: 32401,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [32301],
		isBaseSkill: false,
		isSphereSkill: false
	},
	NEMO: {
		skillId: 32402,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [32301],
		isBaseSkill: false,
		isSphereSkill: false
	},
	CLEPTOMANE: {
		skillId: 32403,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [32302],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ABYSSE: {
		skillId: 32404,
		type: SkillType.A,
		energy: Energy.HIGH,
		element: [ElementType.WATER],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [32303],
		isBaseSkill: false,
		isSphereSkill: false
	},
	BANNI_DES_DIEUX: {
		skillId: 32405,
		type: SkillType.E,
		energy: Energy.WEAK,
		element: [ElementType.WATER],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [32303],
		isBaseSkill: false,
		isSphereSkill: false
	},
	TOURBILLON_MAGIQUE: {
		skillId: 32501,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [32401],
		isBaseSkill: false,
		isSphereSkill: false
	},
	HYPERVENTILATION: {
		skillId: 32502,
		type: SkillType.A,
		energy: Energy.VHIGH,
		element: [ElementType.WATER],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [32402],
		isBaseSkill: false,
		isSphereSkill: false
	},
	THERAPIE_DE_GROUPE: {
		skillId: 32503,
		type: SkillType.E,
		energy: Energy.VHIGH,
		element: [ElementType.WATER],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [32403],
		isBaseSkill: false,
		isSphereSkill: false
	},
	RECEPTACLE_TESLA: {
		skillId: 32504,
		type: SkillType.A,
		energy: Energy.HIGH,
		element: [ElementType.WATER],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [32404],
		isBaseSkill: false,
		isSphereSkill: false
	},
	VITALITE_MARINE: {
		skillId: 32505,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WATER],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [32405],
		isBaseSkill: false,
		isSphereSkill: false,
		effects: [
			{
				type: SkillEffect.CHANGE_MAX_LIFE,
				value: 80
			}
		]
	},
	// LIGHTNING Skills
	INTELLIGENCE: {
		skillId: 41101,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [],
		isBaseSkill: false,
		isSphereSkill: false
	},
	FOCUS: {
		skillId: 41102,
		type: SkillType.E,
		energy: Energy.WEAK,
		element: [ElementType.LIGHTNING],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [],
		isBaseSkill: false,
		isSphereSkill: false
	},
	CELERITE: {
		skillId: 41103,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [],
		isBaseSkill: false,
		isSphereSkill: false
	},
	REFLEX: {
		skillId: 41104,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [],
		isBaseSkill: false,
		isSphereSkill: true
	},
	CONCENTRATION: {
		skillId: 41201,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41102],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ATTAQUE_ECLAIR: {
		skillId: 41202,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41103],
		isBaseSkill: false,
		isSphereSkill: false
	},
	PARATONNERRE: {
		skillId: 41203,
		type: SkillType.C,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41101],
		isBaseSkill: false,
		isSphereSkill: false
	},
	COUP_DOUBLE: {
		skillId: 41204,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41103],
		isBaseSkill: false,
		isSphereSkill: false
	},
	REGENERESCENCE: {
		skillId: 41205,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41102],
		isBaseSkill: false,
		isSphereSkill: false
	},
	PREMIERS_SOINS: {
		skillId: 41206,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41101],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ECLAIR_SINUEUX: {
		skillId: 41207,
		type: SkillType.A,
		energy: Energy.VHIGH,
		element: [ElementType.LIGHTNING],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41104],
		isBaseSkill: false,
		isSphereSkill: true
	},
	FOUDRE: {
		skillId: 41301,
		type: SkillType.A,
		energy: Energy.NORMAL,
		element: [ElementType.LIGHTNING],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41204],
		isBaseSkill: false,
		isSphereSkill: false
	},
	FISSION_ELEMENTAIRE: {
		skillId: 41302,
		type: SkillType.C,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41203],
		isBaseSkill: false,
		isSphereSkill: false
	},
	VOIE_DE_KAOS: {
		skillId: 41303,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41201],
		isBaseSkill: false,
		isSphereSkill: false
	},
	PLAN_DE_CARRIERE: {
		skillId: 41304,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41204],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ADRENALINE: {
		skillId: 41305,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41206],
		isBaseSkill: false,
		isSphereSkill: false
	},
	VOIE_DE_GAIA: {
		skillId: 41306,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41201],
		isBaseSkill: false,
		isSphereSkill: false
	},
	MEDECINE: {
		skillId: 41307,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41206],
		isBaseSkill: false,
		isSphereSkill: false
	},
	DANSE_FOUDROYANTE: {
		skillId: 41308,
		type: SkillType.A,
		energy: Energy.HIGH,
		element: [ElementType.LIGHTNING],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41204],
		isBaseSkill: false,
		isSphereSkill: false
	},
	EMBUCHE: {
		skillId: 41309,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41202],
		isBaseSkill: false,
		isSphereSkill: false
	},
	PUREE_SALVATRICE: {
		skillId: 41310,
		type: SkillType.E,
		energy: Energy.HIGH,
		element: [ElementType.LIGHTNING],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41205],
		isBaseSkill: false,
		isSphereSkill: false
	},
	AURA_HERMETIQUE: {
		skillId: 41311,
		type: SkillType.E,
		energy: Energy.WEAK,
		element: [ElementType.LIGHTNING],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41205],
		isBaseSkill: false,
		isSphereSkill: false
	},
	CROCS_DIAMANT: {
		skillId: 41312,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41203],
		isBaseSkill: false,
		isSphereSkill: false
	},
	SURVIE: {
		skillId: 41313,
		type: SkillType.S,
		energy: Energy.VHIGH,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41207],
		isBaseSkill: false,
		isSphereSkill: true
	},
	AUBE_FEUILLUE: {
		skillId: 41401,
		type: SkillType.A,
		energy: Energy.VHIGH,
		element: [ElementType.LIGHTNING],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41306],
		isBaseSkill: false,
		isSphereSkill: false
	},
	BRANCARDIER: {
		skillId: 41402,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41307],
		isBaseSkill: false,
		isSphereSkill: false
	},
	BENEDICTION: {
		skillId: 41403,
		type: SkillType.E,
		energy: Energy.WEAK,
		element: [ElementType.LIGHTNING],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41310],
		isBaseSkill: false,
		isSphereSkill: false
	},
	CREPUSCULE_FLAMBOYANT: {
		skillId: 41404,
		type: SkillType.A,
		energy: Energy.HIGH,
		element: [ElementType.LIGHTNING],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41303],
		isBaseSkill: false,
		isSphereSkill: false
	},
	MARCHAND: {
		skillId: 41405,
		type: SkillType.U,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41302],
		isBaseSkill: false,
		isSphereSkill: false
	},
	REINCARNATION: {
		skillId: 41406,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41304],
		isBaseSkill: false,
		isSphereSkill: false
	},
	SURCHARGE: {
		skillId: 41408,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING, ElementType.AIR],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51302, 41305, 61119],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ELECTROLYSE: {
		skillId: 41409,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.WATER, ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41301, 31308, 61119],
		isBaseSkill: false,
		isSphereSkill: false
	},
	GOLEM: {
		skillId: 41410,
		type: SkillType.I,
		energy: Energy.VHIGH,
		element: [ElementType.LIGHTNING, ElementType.FIRE],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41302, 11309, 61121],
		raceId: [raceList.ROCKY.raceId],
		isBaseSkill: false,
		isSphereSkill: false
	},
	RAIJIN: {
		skillId: 41411,
		type: SkillType.I,
		energy: Energy.VHIGH,
		element: [ElementType.LIGHTNING, ElementType.AIR],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41301, 51302, 61121],
		raceId: [raceList.PLANAILLE.raceId, raceList.PLANAILLE_DEMON.raceId],
		isBaseSkill: false,
		isSphereSkill: false
	},
	QUETZACOATL: {
		skillId: 41412,
		type: SkillType.I,
		energy: Energy.VHIGH,
		element: [ElementType.LIGHTNING],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41301],
		raceId: [raceList.QUETZU.raceId],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ROI_DES_SINGES: {
		skillId: 41413,
		type: SkillType.I,
		energy: Energy.VHIGH,
		element: [ElementType.LIGHTNING, ElementType.WOOD],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21308, 41308, 61121],
		raceId: [raceList.TOUFUFU.raceId],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ARCHANGE_CORROSIF: {
		skillId: 41501,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41404],
		isBaseSkill: false,
		isSphereSkill: false,
		effects: [
			{
				type: SkillEffect.CHANGE_ELEMENT,
				value: 2,
				element: ElementType.FIRE
			},
			{
				type: SkillEffect.CHANGE_ELEMENT,
				value: 1,
				element: ElementType.LIGHTNING
			}
		]
	},
	ARCHANGE_GENESIF: {
		skillId: 41502,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41401],
		isBaseSkill: false,
		isSphereSkill: false,
		effects: [
			{
				type: SkillEffect.CHANGE_ELEMENT,
				value: 2,
				element: ElementType.WOOD
			},
			{
				type: SkillEffect.CHANGE_ELEMENT,
				value: 1,
				element: ElementType.LIGHTNING
			}
		]
	},
	PRETRE: {
		skillId: 41503,
		type: SkillType.U,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41403],
		isBaseSkill: false,
		isSphereSkill: false
	},
	SOUTIEN_MORAL: {
		skillId: 42101,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [],
		isBaseSkill: false,
		isSphereSkill: false
	},
	STIMULATION_CARDIAQUE: {
		skillId: 42201,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [42101],
		isBaseSkill: false,
		isSphereSkill: false
	},
	JAUNE: {
		skillId: 42202,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [42101],
		isBaseSkill: false,
		isSphereSkill: false
	},
	MORSURE_DU_SOLEIL: {
		skillId: 42301,
		type: SkillType.E,
		energy: Energy.NORMAL,
		element: [ElementType.LIGHTNING],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [42201],
		isBaseSkill: false,
		isSphereSkill: false
	},
	CRAMPE_CHRONIQUE: {
		skillId: 42303,
		type: SkillType.E,
		energy: Energy.WEAK,
		element: [ElementType.LIGHTNING],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [42202],
		isBaseSkill: false,
		isSphereSkill: false
	},
	BATTERIE_SUPPLEMENTAIRE: {
		skillId: 42401,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [42301],
		isBaseSkill: false,
		isSphereSkill: false,
		effects: [
			{
				type: SkillEffect.CHANGE_MAX_LIFE,
				value: 50
			}
		]
	},
	EINSTEIN: {
		skillId: 42402,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [42301],
		isBaseSkill: false,
		isSphereSkill: false
	},
	BARRIERE_ELECTRIFIEE: {
		skillId: 42403,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [42303],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ORACLE: {
		skillId: 42404,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [42303],
		isBaseSkill: false,
		isSphereSkill: false
	},
	RECEPTACLE_AERIEN: {
		skillId: 42501,
		type: SkillType.A,
		energy: Energy.HIGH,
		element: [ElementType.LIGHTNING],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [42401],
		isBaseSkill: false,
		isSphereSkill: false
	},
	FEU_DE_ST_ELME: {
		skillId: 42502,
		type: SkillType.A,
		energy: Energy.HIGH,
		element: [ElementType.LIGHTNING],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [42402],
		isBaseSkill: false,
		isSphereSkill: false
	},
	FORCE_DE_ZEUS: {
		skillId: 42503,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [42403],
		isBaseSkill: false,
		isSphereSkill: false
	},
	REMANENCE_HERTZIENNE: {
		skillId: 42504,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.LIGHTNING],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [42404],
		isBaseSkill: false,
		isSphereSkill: false
	},
	// Air Skills
	AGILITE: {
		skillId: 51101,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [],
		isBaseSkill: false,
		isSphereSkill: false
	},
	STRATEGIE: {
		skillId: 51102,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [],
		isBaseSkill: false,
		isSphereSkill: false
	},
	MISTRAL: {
		skillId: 51103,
		type: SkillType.A,
		energy: Energy.WEAK,
		element: [ElementType.AIR],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [],
		isBaseSkill: false,
		isSphereSkill: false
	},
	AIGUILLON: {
		skillId: 51104,
		type: SkillType.E,
		energy: Energy.HIGH,
		element: [ElementType.AIR],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [],
		isBaseSkill: false,
		isSphereSkill: true
	},
	ENVOL: {
		skillId: 51105,
		type: SkillType.A,
		energy: Energy.WEAK,
		element: [ElementType.AIR],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [],
		raceId: [raceList.PTEROZ.raceId],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ESQUIVE: {
		skillId: 51201,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51101],
		isBaseSkill: false,
		isSphereSkill: false
	},
	SAUT: {
		skillId: 51202,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51101],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ANALYSE: {
		skillId: 51203,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51102],
		isBaseSkill: false,
		isSphereSkill: false
	},
	CUEILLETTE: {
		skillId: 51204,
		type: SkillType.C,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51102],
		isBaseSkill: false,
		isSphereSkill: false
	},
	TAICHI: {
		skillId: 51205,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51103],
		isBaseSkill: false,
		isSphereSkill: false
	},
	TORNADE: {
		skillId: 51206,
		type: SkillType.A,
		energy: Energy.NORMAL,
		element: [ElementType.AIR],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51103],
		isBaseSkill: false,
		isSphereSkill: false
	},
	AURA_PUANTE: {
		skillId: 51207,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51104],
		isBaseSkill: false,
		isSphereSkill: true
	},
	DISQUE_VACUUM: {
		skillId: 51301,
		type: SkillType.A,
		energy: Energy.HIGH,
		element: [ElementType.AIR],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51201],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ELASTICITE: {
		skillId: 51302,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51201],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ATTAQUE_PLONGEANTE: {
		skillId: 51303,
		type: SkillType.A,
		energy: Energy.WEAK,
		element: [ElementType.AIR],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51202],
		isBaseSkill: false,
		isSphereSkill: false
	},
	FURTIVITE: {
		skillId: 51304,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51202],
		isBaseSkill: false,
		isSphereSkill: false
	},
	SPECIALISTE: {
		skillId: 51305,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51203],
		isBaseSkill: false,
		isSphereSkill: false
	},
	TALON_DACHILLE: {
		skillId: 51306,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51203],
		isBaseSkill: false,
		isSphereSkill: false
	},
	NUAGE_TOXIQUE: {
		skillId: 51307,
		type: SkillType.A,
		energy: Energy.HIGH,
		element: [ElementType.AIR],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51204],
		isBaseSkill: false,
		isSphereSkill: false
	},
	OEIL_DE_LYNX: {
		skillId: 51308,
		type: SkillType.C,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51204],
		isBaseSkill: false,
		isSphereSkill: false
	},
	EVEIL: {
		skillId: 51309,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51205],
		isBaseSkill: false,
		isSphereSkill: false
	},
	PAUME_EJECTABLE: {
		skillId: 51310,
		type: SkillType.A,
		energy: Energy.WEAK,
		element: [ElementType.AIR],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51205],
		isBaseSkill: false,
		isSphereSkill: false
	},
	VENT_VIF: {
		skillId: 51311,
		type: SkillType.E,
		energy: Energy.NORMAL,
		element: [ElementType.AIR],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51206],
		isBaseSkill: false,
		isSphereSkill: false
	},
	FORME_VAPOREUSE: {
		skillId: 51312,
		type: SkillType.S,
		energy: Energy.NORMAL,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51206],
		isBaseSkill: false,
		isSphereSkill: false
	},
	HYPNOSE: {
		skillId: 51313,
		type: SkillType.A,
		energy: Energy.VHIGH,
		element: [ElementType.AIR],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51207],
		isBaseSkill: false,
		isSphereSkill: true
	},
	SECOUSSE: {
		skillId: 51314,
		type: SkillType.A,
		energy: Energy.WEAK,
		element: [ElementType.WOOD, ElementType.AIR],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51310, 21304, 61119],
		isBaseSkill: false,
		isSphereSkill: false
	},
	TROU_NOIR: {
		skillId: 51401,
		type: SkillType.A,
		energy: Energy.VHIGH,
		element: [ElementType.AIR],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51301],
		isBaseSkill: false,
		isSphereSkill: false
	},
	MAITRE_LEVITATEUR: {
		skillId: 51402,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51303],
		isBaseSkill: false,
		isSphereSkill: false
	},
	HALEINE_FETIVE: {
		skillId: 51403,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51307],
		isBaseSkill: false,
		isSphereSkill: false
	},
	MEDITATION_SOLITAIRE: {
		skillId: 51404,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51309],
		isBaseSkill: false,
		isSphereSkill: false
	},
	PROFESSEUR: {
		skillId: 51405,
		type: SkillType.U,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51309],
		isBaseSkill: false,
		isSphereSkill: false
	},
	SOUFFLE_DE_VIE: {
		skillId: 51406,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51311],
		isBaseSkill: false,
		isSphereSkill: false
	},
	TOTEM_ANCESTRAL_AEROPORTE: {
		skillId: 51407,
		type: SkillType.I,
		energy: Energy.VHIGH,
		element: [ElementType.AIR, ElementType.FIRE],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11311, 51309, 61121],
		raceId: [raceList.KABUKI.raceId, raceList.KABUKI_DEMON.raceId],
		isBaseSkill: false,
		isSphereSkill: false
	},
	FUJIN: {
		skillId: 51408,
		type: SkillType.I,
		energy: Energy.VHIGH,
		element: [ElementType.AIR, ElementType.LIGHTNING],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41308, 51301, 61121],
		raceId: [raceList.NUAGOZ.raceId],
		isBaseSkill: false,
		isSphereSkill: false
	},
	MEDITATION_TRANCHANTE: {
		skillId: 51501,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51404],
		isBaseSkill: false,
		isSphereSkill: false
	},
	DJINN: {
		skillId: 51502,
		type: SkillType.I,
		energy: Energy.VHIGH,
		element: [ElementType.AIR, ElementType.FIRE],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [11201, 51403, 61121],
		raceId: [raceList.PTEROZ.raceId],
		isBaseSkill: false,
		isSphereSkill: false
	},
	HADES: {
		skillId: 51503,
		type: SkillType.I,
		energy: Energy.VHIGH,
		element: [ElementType.AIR, ElementType.WOOD],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51403, 21204, 31203, 61121],
		raceId: [raceList.SANTAZ.raceId],
		isBaseSkill: false,
		isSphereSkill: false
	},
	FORME_ETHERALE: {
		skillId: 51601,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51501],
		isBaseSkill: false,
		isSphereSkill: false
	},
	MAITRISE_CORPORELLE: {
		skillId: 52101,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [],
		isBaseSkill: false,
		isSphereSkill: false
	},
	BLANC: {
		skillId: 52201,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [52101],
		isBaseSkill: false,
		isSphereSkill: false
	},
	ANAEROBIE: {
		skillId: 52202,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [52101],
		isBaseSkill: false,
		isSphereSkill: false
	},
	DOUBLE_FACE: {
		skillId: 52301,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [52201],
		isBaseSkill: false,
		isSphereSkill: false
	},
	FLAGELLATION: {
		skillId: 52302,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [52201],
		isBaseSkill: false,
		isSphereSkill: false
	},
	SOUFFLE_DANGE: {
		skillId: 52303,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [52202],
		isBaseSkill: false,
		isSphereSkill: false
	},
	OURAGAN: {
		skillId: 52304,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [52202],
		isBaseSkill: false,
		isSphereSkill: false
	},
	OURANOS: {
		skillId: 52401,
		type: SkillType.A,
		energy: Energy.HIGH,
		element: [ElementType.AIR],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [52301],
		isBaseSkill: false,
		isSphereSkill: false
	},
	TWINOID_500MG: {
		skillId: 52402,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [52302],
		isBaseSkill: false,
		isSphereSkill: false
	},
	LONDUHAUT: {
		skillId: 52403,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [52303],
		isBaseSkill: false,
		isSphereSkill: false
	},
	SURPLIS_DHADES: {
		skillId: 52404,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [52304],
		isBaseSkill: false,
		isSphereSkill: false
	},
	RECEPTABLE_THERMIQUE: {
		skillId: 52405,
		type: SkillType.A,
		energy: Energy.HIGH,
		element: [ElementType.AIR],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [52304],
		isBaseSkill: false,
		isSphereSkill: false
	},
	QI_GONG: {
		skillId: 52501,
		type: SkillType.E,
		energy: Energy.VHIGH,
		element: [ElementType.AIR],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [52401],
		isBaseSkill: false,
		isSphereSkill: false
	},
	SYLPHIDES: {
		skillId: 52502,
		type: SkillType.A,
		energy: Energy.VHIGH,
		element: [ElementType.AIR],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [52402],
		isBaseSkill: false,
		isSphereSkill: false
	},
	MESSIE: {
		skillId: 52503,
		type: SkillType.U,
		energy: Energy.NONE,
		element: [ElementType.AIR],
		activatable: false,
		tree: SkillTree.ETHER,
		unlockedFrom: [52403],
		isBaseSkill: false,
		isSphereSkill: false
	},
	MUTINERIE: {
		skillId: 52504,
		type: SkillType.E,
		energy: Energy.HIGH,
		element: [ElementType.AIR],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [52405],
		isBaseSkill: false,
		isSphereSkill: false
	},
	MAINS_COLLANTES: {
		skillId: 52505,
		type: SkillType.E,
		energy: Energy.WEAK,
		element: [ElementType.AIR],
		activatable: true,
		tree: SkillTree.ETHER,
		unlockedFrom: [52404],
		isBaseSkill: false,
		isSphereSkill: false
	},
	// Void Skills
	COMPETENCE_DOUBLE: {
		skillId: 61119,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.VOID],
		activatable: false,
		tree: SkillTree.VANILLA,
		isBaseSkill: false,
		isSphereSkill: false
	},
	LIMITE_BRISEE: {
		skillId: 61120,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.VOID],
		activatable: false,
		tree: SkillTree.VANILLA,
		isBaseSkill: false,
		isSphereSkill: false
	},
	INVOCATEUR: {
		skillId: 61121,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.VOID],
		activatable: false,
		tree: SkillTree.VANILLA,
		isBaseSkill: false,
		isSphereSkill: false
	},
	FRENESIE_COLLECTIVE: {
		skillId: 61101,
		type: SkillType.E,
		energy: Energy.WEAK,
		element: [ElementType.VOID],
		activatable: true,
		tree: SkillTree.VANILLA,
		raceId: [raceList.WANWAN_DEMON.raceId],
		isBaseSkill: true,
		isSphereSkill: false
	},
	COQUE: {
		skillId: 61102,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.VOID],
		activatable: false,
		tree: SkillTree.VANILLA,
		raceId: [raceList.WINKS.raceId],
		isBaseSkill: true,
		isSphereSkill: false
	},
	CHARGE_CORNUE: {
		skillId: 61103,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.VOID],
		activatable: false,
		tree: SkillTree.VANILLA,
		raceId: [raceList.PIGMOU.raceId],
		isBaseSkill: true,
		isSphereSkill: false
	},
	ROCK: {
		skillId: 61104,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.VOID],
		activatable: false,
		tree: SkillTree.VANILLA,
		raceId: [raceList.ROCKY.raceId],
		isBaseSkill: true,
		isSphereSkill: false
	},
	PIETINEMENT: {
		skillId: 61105,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.VOID],
		activatable: false,
		tree: SkillTree.VANILLA,
		raceId: [raceList.SANTAZ.raceId],
		isBaseSkill: true,
		isSphereSkill: false
	},
	CUIRASSE: {
		skillId: 61106,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.VOID],
		activatable: false,
		tree: SkillTree.VANILLA,
		raceId: [raceList.FEROSS.raceId],
		isBaseSkill: true,
		isSphereSkill: false
	},
	INSAISISSABLE: {
		skillId: 61107,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.VOID],
		activatable: false,
		tree: SkillTree.VANILLA,
		raceId: [raceList.KABUKI.raceId, raceList.KABUKI_DEMON.raceId],
		isBaseSkill: true,
		isSphereSkill: false
	},
	DEPLACEMENT_INSTANTANE: {
		skillId: 61108,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.VOID],
		activatable: false,
		tree: SkillTree.VANILLA,
		raceId: [raceList.TOUFUFU.raceId],
		isBaseSkill: true,
		isSphereSkill: false
	},
	NAPOMAGICIEN: {
		skillId: 61109,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.VOID],
		activatable: false,
		tree: SkillTree.VANILLA,
		raceId: [raceList.SOUFFLET.raceId],
		isBaseSkill: true,
		isSphereSkill: false
	},
	GROS_COSTAUD: {
		skillId: 61111,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.VOID],
		activatable: false,
		tree: SkillTree.VANILLA,
		raceId: [raceList.GORILLOZ_DEMON.raceId],
		isBaseSkill: true,
		isSphereSkill: false
	},
	ORIGINE_CAUSHEMESHENNE: {
		skillId: 61112,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.VOID],
		activatable: false,
		tree: SkillTree.VANILLA,
		raceId: [raceList.KABUKI_DEMON.raceId],
		isBaseSkill: true,
		isSphereSkill: false
	},
	ECRASEMENT: {
		skillId: 61113,
		type: SkillType.A,
		energy: Energy.WEAK,
		element: [ElementType.VOID],
		activatable: true,
		tree: SkillTree.VANILLA,
		raceId: [raceList.MAHAMUTI.raceId],
		isBaseSkill: true,
		isSphereSkill: false
	},
	FORCE_DE_LUMIERE: {
		skillId: 61114,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.VOID],
		activatable: false,
		tree: SkillTree.VANILLA,
		raceId: [raceList.MOUEFFE_DEMON.raceId],
		isBaseSkill: true,
		isSphereSkill: false
	},
	CHARGE_PIGMOU: {
		skillId: 61115,
		type: SkillType.A,
		energy: Energy.WEAK,
		element: [ElementType.VOID],
		activatable: true,
		tree: SkillTree.VANILLA,
		raceId: [raceList.PIGMOU_DEMON.raceId],
		isBaseSkill: true,
		isSphereSkill: false
	},
	FORCE_DES_TENEBRES: {
		skillId: 61116,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.VOID],
		activatable: false,
		tree: SkillTree.VANILLA,
		raceId: [raceList.PLANAILLE_DEMON.raceId],
		isBaseSkill: true,
		isSphereSkill: false
	},
	BIGMAGNON: {
		skillId: 61117,
		type: SkillType.S,
		energy: Energy.NONE,
		element: [ElementType.VOID],
		activatable: false,
		tree: SkillTree.VANILLA,
		raceId: [raceList.TRICERAGNON.raceId],
		isBaseSkill: true,
		isSphereSkill: false
	},
	DUR_A_CUIRE: {
		skillId: 61118,
		type: SkillType.P,
		energy: Energy.NONE,
		element: [ElementType.VOID],
		activatable: false,
		tree: SkillTree.VANILLA,
		raceId: [raceList.WINKS_DEMON.raceId],
		isBaseSkill: true,
		isSphereSkill: false
	},
	// Other Skills (?)
	HERCOLUBUS: {
		skillId: 41504,
		type: SkillType.I,
		energy: Energy.VHIGH,
		element: [ElementType.LIGHTNING, ElementType.AIR],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41501, 51312, 61121],
		raceId: [raceList.SMOG.raceId],
		isBaseSkill: false,
		isSphereSkill: false
	},
	REINE_DE_LA_RUCHE: {
		skillId: 41505, //Edit since this
		type: SkillType.I,
		energy: Energy.VHIGH,
		element: [ElementType.AIR, ElementType.WOOD],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [51405, 51311, 61121],
		raceId: [raceList.SOUFFLET.raceId],
		isBaseSkill: false,
		isSphereSkill: false
	},
	BIG_MAMA: {
		skillId: 51506,
		type: SkillType.I,
		energy: Energy.VHIGH,
		element: [ElementType.WOOD, ElementType.FIRE],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [12502, 22504, 61121],
		raceId: [raceList.TRICERAGNON.raceId],
		isBaseSkill: false,
		isSphereSkill: false
	},
	YGGDRASIL: {
		skillId: 41507,
		type: SkillType.I,
		energy: Energy.VHIGH,
		element: [ElementType.WOOD, ElementType.AIR],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [21405, 51204],
		raceId: [raceList.WANWAN.raceId, raceList.WANWAN_DEMON.raceId],
		isBaseSkill: false,
		isSphereSkill: false
	},
	BALEINE_BLANCHE: {
		skillId: 41508,
		type: SkillType.I,
		energy: Energy.VHIGH,
		element: [ElementType.WATER, ElementType.LIGHTNING],
		activatable: true,
		tree: SkillTree.VANILLA,
		unlockedFrom: [41305, 31404],
		raceId: [raceList.WINKS.raceId, raceList.WINKS_DEMON.raceId],
		isBaseSkill: false,
		isSphereSkill: false
	}
};
