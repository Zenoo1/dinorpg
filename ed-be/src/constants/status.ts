export const statusList: Record<string, number> = {
	CLIMBING_GEAR: 2,
	BUOY: 1,
	CUSCOUZ_MALEDICTION: 65,
	DEMON: 63,
	LANTERN: 3,
	NENUPHAR_LEAF: 13,
	SHOVEL: 4,
	ENHANCED_SHOVEL: 5,
	BROKEN_SHOVEL: 6,
	RASCAPHANDRE_DECOY: 22,
	FLIPPERS: 25,
	STRATEGY_IN_130_LESSONS: 34,
	OLD_STONE: 26,
	ASHPOUK_TOTEM: 27,
	BASALT_SHARD: 10,
	PURE_WATER: 9,
	SWAMP_MUD: 8,
	ZORS_GLOVE: 11,
	BACKPACK: 40,
	MEMBERSHIP_CARD: 32,
	GOLD_MEDAL_BEAUTY: 59,
	SILVER_MEDAL_BEAUTY: 60,
	BRONZE_MEDAL_BEAUTY: 61,
	CORAIL: 54,
	BRONZE_CUP: 44,
	SILVER_CUP: 45,
	GOLD_CUP: 46,
	DIAMOND_CUP: 47,
	FLOWERING_BRANCH: 39,
	ICE_PIECE: 55,
	BROKEN_LIMIT_1: 50,
	BROKEN_LIMIT_2: 51,
	BROKEN_LIMIT_3: 52,
	CURSED: 62,
	CATCHING_GLOVE: 66,
	BRONZE_MEDAL_FORCEBRUT: 35,
	SILVER_MEDAL_FORCEBRUT: 36,
	GOLD_MEDAL_FORCEBRUT: 37,
	DIAMOND_MEDAL_FORCEBRUT: 38,
	DARK_ORB: 49,
	ETHER_DROP: 53,
	ANTI_SEDH_POTION: 56,
	REINCARNATION: 41,
	RENAISSANCE: 42,
	SKULLY_MEMORY: 57,
	SYLVENOIRE_KEY: 48,
	VENERABLE: 30,
	FIRE_CHARM: 23,
	WATER_CHARM: 24,
	JOVEBOZE: 67,
	DINOPLAZA: 58,
	BROKEN_ENHANCED_SHOVEL: 7,
	QWHY: 17,
	QWOOD: 16,
	QTAME: 18,
	DIAN: 19,
	SHFLAG: 33,
	FFLAG: 31,
	GRDMIS: 20,
	CHUTES: 14, //NOT NEEDED
	MONISL: 64, //NOT IMPLEMENTED YET
	FSPELE: 43, //NOT IMPLEMENTED YET
	TOURNA: 28, //NOT IMPLEMENTED YET
	VMEM: 29, //NOT IMPLEMENTED YET
	JVBZ: 15,
	ZENBRO: 12,
	HUMISS: 21,
	SPHERE: 68
};
